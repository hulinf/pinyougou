package com.pinyougou.service;

import com.pinyougou.pojo.Address;
import com.pinyougou.pojo.Areas;
import com.pinyougou.pojo.Cities;
import com.pinyougou.pojo.Provinces;

import java.util.List;
import java.io.Serializable;
import java.util.Map;

/**
 * AddressService 服务接口
 * @date 2019-06-05 15:27:27
 * @version 1.0
 */
public interface AddressService {

	/** 添加方法 */
	void save(Address address);

	/** 修改方法 */
	void update(Address address);

	/** 根据主键id删除 */
	void delete(Serializable id);

	/** 批量删除 */
	void deleteAll(Serializable[] ids);

	/** 根据主键id查询 */
	Address findOne(Serializable id);

	/** 查询全部 */
	List<Address> findAll();

	/** 多条件分页查询 */
	List<Address> findByPage(Address address, int page, int rows);

	/**
	 * 根据用户编号查询地址
	 * @param userId 用户编号
	 * @return 地址集合
	 */
	List<Address> findAddressByUser(String userId);

	/**
	 *  修改默认地址
	 * @param id 主键id
	 * @param userId  用户id
	 */
	void updateAddressByIsDefault(Long id, String userId);

	//通过主键id删除地址
	void deleteAddressById(Long id);

	//省
	List<Provinces> findprovincesList();

	//根据省的id 查询城市id
	List<Cities> findCitiesListByProvinceId(String provinceId);

	//根据市的id 查询区列表
	List<Areas> findAreasListByProvinceId(String cityId);

	//添加地址
	void addAddress(Address address,String userId);

	//修改地址
	void updateAddress(Address address);

	//查询省的名称
	Provinces findprovinceName(String provinceId);

	//查询区的名称
	Areas findtownName(String townId);

	//查询市的名称
	Cities findcityName(String cityId);
}