package com.pinyougou.service;

import com.pinyougou.cart.Cart;

import java.util.List; /**
 * CartService接口
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-26<p>
 */
public interface CartService {

    /**
     * 把SKU商品添加到购物车
     * @param carts 购物车集合
     * @param itemId SKU商品的id
     * @param num 购买数量
     * @return 修改后的购物车集合
     */
    List<Cart> addItemToCart(List<Cart> carts, Long itemId, Integer num);

    /** 修改购物车 */
    List<Cart> updateCart(List<Cart> carts, Long itemId, Integer num);

    /** 用户购物车存储到Redis */
    void saveCartRedis(String userId, List<Cart> carts);

    /** 从Redis数据库获取购物车 */
    List<Cart> findCartRedis(String userId);

    /**
     * 购物车合并
     * @param cookieCarts Cookie中的购物车
     * @param redisCarts Redis中的购物车
     * @return 合并后的购物车
     */
    List<Cart> mergeCart(List<Cart> cookieCarts, List<Cart> redisCarts);
}
