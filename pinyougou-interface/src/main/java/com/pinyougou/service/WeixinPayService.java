package com.pinyougou.service;

import java.util.Map;

/**
 * WeixinPayService接口
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-29<p>
 */
public interface WeixinPayService {


    /**
     * 【生成支付二维码】
     * 调用微信支付系统“统一下单接口”
     * 获取支付URL: code_url
     */
    Map<String,Object> genPayCode(String outTradeNo, String totalFee);

    /**
     * 【判断交易状态码】
     * 调用微信支付系统"查询订单接口”
     * 获取交易状态码: trade_state
     */
    Map<String,String> queryPayStatus(String outTradeNo);


    /**
     * 【判断订单关闭状态码】
     * 调用微信支付系统"关闭订单接口”
     * 获取关闭状态码: result_code
     */
    Map<String,String> closePayTimeout(String outTradeNo);
}
