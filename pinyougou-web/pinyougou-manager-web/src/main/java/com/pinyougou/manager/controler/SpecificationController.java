package com.pinyougou.manager.controler;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.common.pojo.PageResult;
import com.pinyougou.pojo.Specification;
import com.pinyougou.pojo.SpecificationOption;
import com.pinyougou.service.SpecificationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 规格控制器
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-06<p>
 */
@RestController
@RequestMapping("/specification")
public class SpecificationController {

    @Reference(timeout = 10000)
    private SpecificationService specificationService;

    /** 多条件分页查询规格 */
    @GetMapping("/findByPage")
    public PageResult findByPage(Specification specification, Integer page,
                                 @RequestParam(defaultValue = "10")Integer rows){
        // 1. get请求中文转码
        try {
            if (StringUtils.isNoneBlank(specification.getSpecName())){
                specification.setSpecName(new String(specification
                        .getSpecName().getBytes("ISO8859-1"), "UTF-8"));
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        // 2. 调用服务接口
        return specificationService.findByPage(specification, page, rows);
    }

    /** 添加规格选项 */
    @PostMapping("/save")
    public boolean save(@RequestBody Specification specification){
        try {
            specificationService.save(specification);
            return true;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return false;
    }

    /** 根据规格id查询规格选项集合 */
    @GetMapping("/findSpecOption")
    public List<SpecificationOption> findSpecOption(Long specId){
        return specificationService.findSpecOption(specId);
    }

    /** 修改规格选项 */
    @PostMapping("/update")
    public boolean update(@RequestBody Specification specification){
        try {
            specificationService.update(specification);
            return true;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return false;
    }


    /** 删除规格选项 */
    @GetMapping("/delete")
    public boolean delete(Long[] ids){
        try {
            specificationService.deleteAll(ids);
            return true;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return false;
    }

    /** 查询规格 */
    @GetMapping("/findSpecList")
    public List<Map<String, Object>> findSpecList(){
        return specificationService.findAllByIdAndName();
    }
}
