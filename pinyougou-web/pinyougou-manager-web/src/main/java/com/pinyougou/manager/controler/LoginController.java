package com.pinyougou.manager.controler;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 登录控制器
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-09<p>
 */
@Controller
@RequestMapping("/user")
public class LoginController {

    /** 登录方法 */
    @PostMapping("/login")
    public String login(String username, String password){
        // 获取认证主体
        Subject subject = SecurityUtils.getSubject();
        // 创建用户名与密码令牌
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        try{
            // 身份认证(登录)
            subject.login(token);
            // 判断用户是否认证成功
            if (subject.isAuthenticated()){
                return "redirect:/admin/index.html";
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }

        return "redirect:/login.html";
    }

    /** 获取登录用户名 */
    @GetMapping("/findLoginName")
    @ResponseBody
    public String findLoginName(){
        // 获取登录用户名
        return (String)SecurityUtils.getSubject().getPrincipal();
    }
}
