// 监听文档加载完
window.onload = function () {
    // 创建Vue对象
    var vue = new Vue({
        el : '#app', // 元素绑定
        data : { // 数据模型
            dataList : [], // 品牌数组
            entity : {},   // 封装表单数据
            pages : 0,  // 总页数
            page : 1,      // 当前页码
            searchEntity : {}, // 封装查询条件
            ids : [], // 封装删除的品牌id
            checked : false // 全选是否选中
        },
        methods : { // 操作方法
            // 查询全部品牌
            search : function (page) {
                // 发送异步请求
                axios.get("/brand/findByPage?page=" + page,
                    {params : this.searchEntity}).then(function(response){
                    // 获取响应数据 response.data: {pages : 100, rows : [{},{}]}
                    // 分页数据
                    vue.dataList = response.data.rows;
                    // 总页数
                    vue.pages = response.data.pages;
                    // 当前页码
                    vue.page = page;
                    // 清空ids
                    vue.ids = [];

                });
            },
            // 添加或修改品牌
            saveOrUpdate : function () {
                var url = "save"; // 添加
                // 判断id是否存在
                if (this.entity.id){ // 存在
                    url = "update";
                }
                axios.post("/brand/" + url, this.entity).then(function(response){
                    // response.data: true|false
                    if (response.data){
                        // 操作成功,重新加载数据
                        vue.search(vue.page);
                    }else {
                        alert("操作失败！");
                    }
                });
            },
            // 修改按钮事件
            show :function (entity) {
                // 把json对象转化成json字符串
                var jsonStr = JSON.stringify(entity);
                // 把json字符串解析成新的json对象
                this.entity = JSON.parse(jsonStr);
            },
            // 全选点击事件
            checkAll : function (e) {
                // 清空ids数组
                this.ids = [];
                // 判断全选checkbox是否选中
                if(e.target.checked){
                    for (var i = 0; i < this.dataList.length; i++){
                        // 往数组中添加元素
                        this.ids.push(this.dataList[i].id);
                    }
                }
            },
            // 删除按钮
            del : function () {
                if (this.ids.length > 0){
                    axios.get("/brand/delete?ids=" + this.ids).then(function(response){
                        // 获取响应数据 true|false
                        if (response.data){
                            // 计算出页码
                            var page = (vue.checked && vue.page == vue.pages) ? vue.page - 1 : vue.page;
                            page = page < 1 ? 1 : page;
                            // 重新查询数据
                            vue.search(page);
                        }else{
                            alert("删除失败！");
                        }
                    });
                }else {
                    alert("请选择要删除的品牌！s");
                }
            }
        },
        created : function () { // 创建Vue对象之后(初始化方法)
            // 查询全部品牌
            this.search(this.page);
        },
        updated : function () { // 更新数据之后(数据模型data中变量发生改变之后)
            if (this.dataList.length > 0) {
                this.checked = this.dataList.length == this.ids.length;
            }
        }
    });
};