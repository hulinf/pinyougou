package com.pinyougou.search.listener;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.pinyougou.es.EsItem;
import com.pinyougou.pojo.Item;
import com.pinyougou.service.GoodsService;
import com.pinyougou.service.ItemSearchService;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 商品索引数据同步的消息监听器
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-21<p>
 */
public class ItemMessageListener implements MessageListenerConcurrently {

    @Reference(timeout = 10000)
    private GoodsService goodsService;
    @Reference(timeout = 10000)
    private ItemSearchService itemSearchService;

    @Override
    public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> messageExts,
                      ConsumeConcurrentlyContext context) {
        try {
            // 1. 接收消息
            MessageExt messageExt = messageExts.get(0);
            // 1.1 获取标签
            String tags = messageExt.getTags();
            // 1.2 获取消息内容，把消息内容转化成集合
            List<Long> goodsIds = JSON.parseArray(new
                    String(messageExt.getBody(),"UTF-8"),Long.class);
            System.out.println("goodsIds = " + goodsIds);

            if ("UPDATE".equals(tags)){ // 生成商品索引
                // 2. 根据goodsIds查询上架通过的SKU商品数据
                List<Item> itemList = goodsService.findItemByGoodsId(goodsIds);

                // 3. 把List<Item> 转化成 List<EsItem> 集合
                List<EsItem> esItems = new ArrayList<>();
                for (Item item : itemList) {
                    // 把Item转化成EsItem
                    EsItem esItem = new EsItem();
                    esItem.setId(item.getId());
                    esItem.setTitle(item.getTitle());
                    esItem.setPrice(item.getPrice().doubleValue());
                    esItem.setImage(item.getImage());
                    esItem.setGoodsId(item.getGoodsId());
                    esItem.setCategory(item.getCategory());
                    esItem.setBrand(item.getBrand());
                    esItem.setSeller(item.getSeller());
                    // {"网络":"联通4G","机身内存":"64G"}
                    // 用jackson把json字符串转化成map集合
                    Map specMap = JSON.parseObject(item.getSpec(), Map.class);
                    // 嵌套Field
                    esItem.setSpec(specMap);
                    esItem.setUpdateTime(item.getUpdateTime());

                    esItems.add(esItem);
                }

                // 4. 调用搜索服务接口同步商品的索引
                itemSearchService.saveOrUpdate(esItems);
            }

            if ("DELETE".equals(tags)){ // 删除商品索引
                itemSearchService.delete(goodsIds);
            }

        }catch (Exception ex){
            // 消费失败
            return ConsumeConcurrentlyStatus.RECONSUME_LATER;
        }
        // 消费成功
        return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
    }
}
