// 窗口加载完
window.onload = function () {


    var vue = new Vue({
        el : '#app', // 元素绑定
        data : { // 数据模型
            searchParam : {keywords : '', category : '', brand : '',
                spec : {}, price : '', page : 1,
                sortField : '', sortValue : ''}, // 封装搜索条件
            resultMap : {}, // 搜索数据
            pageNums : [], // 页码数组
            keywords : '',  // 搜索条件
            jumpNum : 1, // 跳转的页码
            firstDot : true, // 前面加点
            lastDot : true // 后面加点
        },
        methods : { // 操作方法
            // 搜索方法
            search : function () {
                // 发送异步请求
                axios.post("/search", this.searchParam).then(function(response){
                    // 获取响应数据 response.data: {"total" : 100, "rows" : [{},{}]}
                    vue.resultMap = response.data;
                    vue.keywords = vue.searchParam.keywords;
                    // 调用初始化页码的方法
                    vue.initPageNums();
                });
            },
            // 初始化页码
            initPageNums : function () {
                // 重新清空
                this.pageNums = [];
                // 定义开始页码
                var firstPage = 1;
                // 定义结束页码
                var lastPage = this.resultMap.totalPages;

                this.firstDot = true; // 前面加点
                this.lastDot =  true; // 后面加点

                // 判断总页数是否大于5
                if (this.resultMap.totalPages > 5){
                    // 判断当前页码靠前面近些
                    if (this.searchParam.page <= 3){
                        lastPage = 5;
                        this.firstDot = false; // 前面不加点
                    }else if (this.searchParam.page >= this.resultMap.totalPages - 3){
                        // 判断当前页码靠后面近些
                        firstPage = this.resultMap.totalPages - 4; // 100 - 4
                        this.lastDot =  false; // 后面不加点
                    }else {
                        // 中间位置
                        firstPage = this.searchParam.page - 2;
                        lastPage = this.searchParam.page + 2;
                    }
                }else{
                    this.firstDot = false; // 前面不加点
                    this.lastDot =  false; // 后面不加点
                }
                // 循环生成页面
                for (var i = firstPage; i <= lastPage; i++){
                    this.pageNums.push(i);
                }
            },
            // 添加过滤条件
            addSearchItem : function (key, value) {
                // 判断是否为：商品分类、品牌、价格
                if (key == 'category' || key == 'brand' || key == 'price'){
                    this.searchParam[key] = value;
                }else {
                    // 规格选项
                    this.searchParam.spec[key] = value;
                }
                // 执行搜索
                this.search();
            },
            // 删除过滤条件
            removeSearchItem : function (key) {
                // 判断是否为：商品分类、品牌、价格
                if (key == 'category' || key == 'brand' || key == 'price'){
                    this.searchParam[key] = "";
                }else {
                    // 规格选项
                    delete this.searchParam.spec[key];
                }
                // 执行搜索
                this.search();
            },
            // 分页搜索
            pageSearch : function (num) {
                // v-model绑定的数据都是string类型
                var num = parseInt(num);
                // 判断页码是否有效
                if (num >= 1 && num <= this.resultMap.totalPages
                    && num != this.searchParam.page){
                    this.searchParam.page = num;
                    this.jumpNum = num;
                    // 执行搜索
                    this.search();
                }
            },
            // 排序搜索
            sortSearch : function (sortField, sortValue) {
                this.searchParam.sortField = sortField;
                this.searchParam.sortValue = sortValue;
                // 执行搜索
                this.search();
            },
            // 初始化搜索方法
            initSearch : function () {
                // http://search.pinyougou.com/?keywords=%E5%B0%8F%E7%B1%B3
                var keywords = this.getUrlParam("keywords");
                this.searchParam.keywords = keywords;
                // 调用搜索方法
                this.search();
            }
        },
        created : function () { // 创建生命周期
            // 初始化搜索方法
            this.initSearch();
        }
    });
};