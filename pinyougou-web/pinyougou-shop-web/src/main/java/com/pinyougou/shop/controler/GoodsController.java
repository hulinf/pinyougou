package com.pinyougou.shop.controler;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.pinyougou.common.pojo.PageResult;
import com.pinyougou.pojo.Goods;
import com.pinyougou.service.GoodsService;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.client.producer.MQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 商品控制器
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-11<p>
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Reference(timeout = 10000)
    private GoodsService goodsService;
    @Autowired
    private MQProducer mqProducer;

    /** 添加商品 */
    @PostMapping("/save")
    public boolean save(@RequestBody Goods goods){
        try{
            // 获取登录用户名
            String sellerId = (String)SecurityUtils.getSubject().getPrincipal();
            // 设置商家的id
            goods.setSellerId(sellerId);
            // 保存商品
            goodsService.save(goods);
            return true;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return false;
    }

    /** 多条件分页查询商家的全部商品 */
    @GetMapping("/findByPage")
    public PageResult findByPage(Goods goods, Integer page,
                                 @RequestParam(defaultValue = "10")Integer rows){
        // 查询登录商家的商品
        String sellerId = (String)SecurityUtils.getSubject().getPrincipal();
        // 设置商家id
        goods.setSellerId(sellerId);

        /** GET请求中文转码 */
        if (StringUtils.isNoneBlank(goods.getGoodsName())) {
            try {
                goods.setGoodsName(new String(goods
                        .getGoodsName().getBytes("ISO8859-1"), "UTF-8"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return goodsService.findByPage(goods, page, rows);

    }

    /** 商品上下架 */
    @GetMapping("/updateMarketable")
    public boolean updateMarketable(Long[] ids, String status){
        try {
            goodsService.updateStatus("is_marketable", ids, status);

            // 判断商品上下架
            if("1".equals(status)){ // 上架
                // 发送消息到MQ服务器，同步商品的索引
                mqProducer.send(new Message("ES_ITEM_TOPIC", "UPDATE",
                        JSON.toJSONString(ids).getBytes("UTF-8")));

                // 发送消息到MQ服务器，生成商品的静态页面
                mqProducer.send(new Message("PAGE_ITEM_TOPIC", "CREATE",
                        JSON.toJSONString(ids).getBytes("UTF-8")));

            }else{ // 下架
                // 发送消息到MQ服务器，删除商品的索引
                mqProducer.send(new Message("ES_ITEM_TOPIC", "DELETE",
                        JSON.toJSONString(ids).getBytes("UTF-8")));

                // 发送消息到MQ服务器，删除静态页面
                mqProducer.send(new Message("PAGE_ITEM_TOPIC", "DELETE",
                        JSON.toJSONString(ids).getBytes("UTF-8")));
            }
            return true;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return false;
    }

}
