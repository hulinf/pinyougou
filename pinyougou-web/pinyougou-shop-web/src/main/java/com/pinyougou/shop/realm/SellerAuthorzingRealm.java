package com.pinyougou.shop.realm;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pojo.Seller;
import com.pinyougou.service.SellerService;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;

/**
 * 自定义的认证域
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-09<p>
 */
public class SellerAuthorzingRealm extends AuthorizingRealm {

    @Reference(timeout = 10000)
    private SellerService sellerService;


    /** 授权方法 */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return null;
    }
    /** 身份认证方法(subject.login()) */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken)
            throws AuthenticationException {

        System.out.println("sellerService = " + sellerService);
        // 1. 获取登录用户名
        String sellerId = (String)authenticationToken.getPrincipal();

        // 2. 从商家表查询数据
        Seller seller = sellerService.findOne(sellerId);

        // 3. 判断商家对象与审核状态码
        if (seller != null && "1".equals(seller.getStatus())){
            // 密码交给Shrio判断
            return new SimpleAuthenticationInfo(sellerId, seller.getPassword(),
                    ByteSource.Util.bytes(seller.getSellerId()), this.getName());
        }

        return null;
    }
}
