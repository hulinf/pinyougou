package com.pinyougou.shop.controler;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pojo.Seller;
import com.pinyougou.service.SellerService;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商家控制器
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-09<p>
 */
@RestController
@RequestMapping("/seller")
public class SellerController {

    @Reference(timeout = 10000)
    private SellerService sellerService;

    /** 商家申请入驻 */
    @PostMapping("/save")
    public boolean save(@RequestBody Seller seller){
        try {
            // MD5(明文 + Md5(随机盐)) 5
            // 商家的密码加密 sha-256 sha-512
            String password = new SimpleHash("md5", seller.getPassword(),
                    seller.getSellerId(), 5).toHex();
            seller.setPassword(password);

            sellerService.save(seller);
            return true;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return false;
    }

    public static void main(String[] args){
        String password = new SimpleHash("md5", "123456","itcast", 5).toHex();
        System.out.println("password = " + password);
    }
}
