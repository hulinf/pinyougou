// 窗口加载完
$(function(){
    // 创建Vue对象
    var vue = new Vue({
        el : '#app', // 元素绑定
        data : { // 数据模型
            goods : {goodsDesc : {itemImages : [], customAttributeItems : [], specificationItems : []},
                     category1Id : '',
                     category2Id : '',
                     category3Id : '',
                     typeTemplateId : '',
                     brandId : '',
                     items : [],
                     isEnableSpec : '0'}, // 数据封装对象(表单)
            picEntity : {url : '',color : ''}, // 商品图片
            itemCat1List : [], // 商品的一级分类
            itemCat2List : [], // 商品的二级分类
            itemCat3List : [],  // 商品的三级分类
            brandList : [], // 品牌数据
            specList : []   // 规格选项数据
        },
        methods : { // 定义操作方法
            saveOrUpdate : function () { // 添加或修改
                // 获取富文本编辑器中的内容
                this.goods.goodsDesc.introduction = editor.html();

                // 发送异步请求
                axios.post("/goods/save", this.goods)
                    .then(function(response){
                    // 获取响应数据
                    if (response.data){ // 操作成功
                        // 清空表单数据
                        vue.goods = {goodsDesc : {itemImages : [],
                                customAttributeItems : [], specificationItems : []},
                                category1Id : '',
                                category2Id : '',
                                category3Id : '',
                                typeTemplateId : '',
                                brandId : '',
                                items : []};
                        // 清空富文本编辑器的内容
                        editor.html("");
                    }else {
                        alert('操作失败！');
                    }
                });
            },
            // 文件异步上传
            uploadFile : function () {

                // html5中的函数
                // 创建表单数据对象
                var formData = new FormData();
                // 表单中追加文件上传的html元素
                // 第一个参数：请求参数
                // 第二个参数：上传的文件 <input type='file'/>
                formData.append("file", file.files[0]);
                // 异步请求上传图片
                axios({
                    method : 'post', // 请求方式
                    url : '/upload', // 请求url
                    data : formData, // 文件上传的数据
                    headers : {"Content-Type" : "multipart/form-data"} // 设置请求头(文件上传)
                }).then(function(response){
                    // 获取响应数据 response.data {status : 200, url : ''}
                    if (response.data.status == 200){
                        vue.picEntity.url = response.data.url;
                    }else{
                        alert("上传失败！");
                    }

                });
            },
            // 添加图片到图片数组
            addPic : function () {
                this.goods.goodsDesc.itemImages.push(this.picEntity);
            },
            // 从图片数组中删除图片
            removePic : function (idx) {
                // 发送异步请求，删除FastDFS服务器中的图片
                this.goods.goodsDesc.itemImages.splice(idx,1);
            },
            // 根据父级id查询商品分类
            findItemCatByParentId : function (parentId, name) {
                axios.get("/itemCat/findItemCatByParentId?parentId="
                    + parentId).then(function(response){
                        // 获取响应数据 List<ItemCat> [{},{}]
                       vue[name] = response.data;
                });
            },
            // 保存用户选中的规格选项
            selectSpecAttr : function (e, specName,optionName) {
                /**
                 * goods.goodsDesc.specificationItems:
                 * [{"attributeValue":["联通4G","移动4G","电信4G"],"attributeName":"网络"},
                 *  {"attributeValue":["64G","128G"],"attributeName":"机身内存"}]
                 */
                // obj:  {"attributeValue":["64G","128G"],"attributeName":"机身内存"}
                var obj = this.searchJsonByKey(this.goods
                    .goodsDesc.specificationItems, "attributeName", specName);
                if (obj){
                    // "attributeValue":["64G","128G"]
                    if (e.target.checked){ // 选中
                        obj.attributeValue.push(optionName);
                    }else{
                        // 获取optionName在attributeValue中索引号
                        var idx = obj.attributeValue.indexOf(optionName);
                        obj.attributeValue.splice(idx,1);
                        // 判断数组长度
                        if (obj.attributeValue.length == 0){
                            // { "attributeValue": [], "attributeName": "机身内存" }
                            idx = this.goods.goodsDesc.specificationItems.indexOf(obj);
                            this.goods.goodsDesc.specificationItems.splice(idx,1);
                        }
                    }
                }else{
                    this.goods.goodsDesc.specificationItems
                        .push({attributeValue : [optionName], attributeName : specName});
                }
            },
            // 根据json的key查询一个json对象，再返回
            searchJsonByKey : function (jsonArr, key, value) {
                for (var i = 0; i < jsonArr.length; i++){
                    // {"attributeValue":["联通4G","移动4G","电信4G"],"attributeName":"网络"}
                    var json = jsonArr[i];
                    if (json[key] == value){
                        return json;
                    }
                }
                return null;
            },
            // 生成SKU商品数组
            createItems : function () {
                // 1. 定义SKU数组变量，并初始化
                this.goods.items = [{spec:{}, price:0, num:9999,
                        status:'0', isDefault:'0'}];
                // 2. 获取用户选中的规格选项
                // [{"attributeValue": [ "移动4G", "联通3G", "联通4G" ], "attributeName": "网络" } ]
                var specItems = this.goods.goodsDesc.specificationItems;

                // 3. 循环规格选项数组生成SKU数组
                for (var i = 0; i < specItems.length; i++){
                    // 3.1 获取一个数组元素
                    // {"attributeValue": [ "移动4G", "联通3G", "联通4G" ], "attributeName": "网络" }
                    var specItem = specItems[i];
                    // 4.2 对原来的SKU数组进行扩充，返回一个新的SKU数组
                    this.goods.items = this.swapItems(this.goods.items,
                        specItem.attributeValue, specItem.attributeName);
                }

            },
            // 转换SKU数组的方法
            swapItems : function (items, attributeValue, attributeName) {
                // items : [{spec:{}, price:0, num:9999,status:'0', isDefault:'0'}]
                // attributeValue: [ "移动4G", "联通3G", "联通4G" ]
                // attributeName : 网络
                // 定义新的SKU数组
                var newItems = new Array();
                // 迭代items
                for (var i = 0; i < items.length; i++){ // 1
                    // 获取一个数组元素
                    //{spec:{}, price:0, num:9999,status:'0', isDefault:'0'}
                    var item = items[i];

                    // 循环规格选项数组 [ "移动4G", "联通3G", "联通4G" ]
                    for (var j = 0; j < attributeValue.length; j++){ // 3
                        // 克隆item,产生新的item
                        var newItem = JSON.parse(JSON.stringify(item));
                        // spec:{}
                        // {"网络":"电信4G","机身内存":"128G"}
                        newItem.spec[attributeName] = attributeValue[j];
                        // 添加到新的SKU数组
                        newItems.push(newItem);
                    }
                }
                return newItems;
            }
        },
        watch : { // watch :它可以监控data中的所有变量，当数据发生改变就会调用一个函数
            // 监控商品的一级分类发生改变，查询商品的二级分类
            "goods.category1Id" : function (newVal, oldVal) {
                this.goods.category2Id = "";
                //alert("新值：" + newVal + ",旧值: " + oldVal);
                if (newVal){ // 不是空 0、null、undefined、""
                    // 发送异步请求查询商品的二级分类
                    vue.findItemCatByParentId(newVal, 'itemCat2List');
                }else{
                    vue.itemCat2List = [];
                }
            },
            // 监控商品的二级分类发生改变，查询商品的三级分类
            "goods.category2Id" : function (newVal, oldVal) {
                this.goods.category3Id = "";
                if (newVal){ // 不是空 0、null、undefined、""
                    // 发送异步请求查询商品的二级分类
                    vue.findItemCatByParentId(newVal, 'itemCat3List');
                }else{
                    vue.itemCat3List = [];
                }
            },
            // 监控商品的三级分类发生改变，查询类型模板id
            "goods.category3Id" : function (newVal, oldVal) {
                this.goods.typeTemplateId = "";
                if (newVal){ // 不是空 0、null、undefined、""
                    // 迭代商品的三级分类数组
                    for (var i = 0; i < this.itemCat3List.length; i++){
                        // 获取数组中的元素
                        var obj = this.itemCat3List[i];
                        // 判断是否选择了该元素
                        if (obj.id == newVal){
                            this.goods.typeTemplateId = obj.typeId;
                            break;
                        }
                    }
                }
            },
            // 监控类型模板id，查询类型模板对象
            "goods.typeTemplateId" : function (newVal, oldVal) {
                this.goods.brandId = "";
                if (newVal){ // 不是空 0、null、undefined、""
                    // 查询类型模板对象
                    axios.get("/typeTemplate/findOne?id=" + newVal).then(function(response){
                        // response.data : {}
                        // 获取品牌数据
                        vue.brandList = JSON.parse(response.data.brandIds);
                        // 获取扩展属性
                        vue.goods.goodsDesc.customAttributeItems =
                            JSON.parse(response.data.customAttributeItems);
                    });
                    // 查询规格选项数据
                    axios.get("/typeTemplate/findSpecOptions?id=" + newVal).then(function(response) {
                        // 获取响应数据
                        /**
                         * [{"id":27,"text":"网络", "options":[{optionName : ''},{optionName : ''}]},
                           {"id":32,"text":"机身内存","options":[{optionName : ''},{optionName : ''}]}]
                         */
                        vue.specList = response.data;
                    });
                }else {
                    // 清空数据
                    this.brandList = [];
                    this.goods.goodsDesc.customAttributeItems = [];
                    this.specList = [];
                }
            }

        },
        created : function () { // 创建生命周期(初始化方法)
            // 查询商品一级分类
            this.findItemCatByParentId(0, 'itemCat1List');
        }
    });
});