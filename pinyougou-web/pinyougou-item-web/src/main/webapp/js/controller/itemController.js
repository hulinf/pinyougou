// 窗口加载完
window.onload = function () {
    var vue = new Vue({
        el : '#app', // 元素绑定
        data : { // 数据模型
            num : 1, // 购买数量
            spec : {}, // 记录的用户选择的规格选项
            sku : {} // SKU商品
        },
        methods : { // 操作方法
            // 购买数量加减
            addNum : function (x) {
                this.num = parseInt(this.num);
                this.num += x;
                if (this.num < 1){
                    this.num = 1;
                }
            },
            // 记录用户选择的规格选项(v-model、异步请求响应数据)
            selecSpec : function (key, value) {
                // 为json对象设置值
                Vue.set(this.spec, key, value);
                // 搜索SKU
                this.searchSku();
            },
            // 判断规格选项是否选中
            isSelected : function (key, value) {
                return this.spec[key] == value;
            },
            // 加载默认的sku
            loadSku : function () {
                // 从SKU数组中取第一个元素
                this.sku = itemList[0];
                this.spec = JSON.parse(this.sku.spec);
            },
            // 搜索SKU
            searchSku : function () {
                // 迭代SKU数组
                for (var i = 0; i < itemList.length; i++){
                    var item = itemList[i];
                    if (JSON.stringify(this.spec) == item.spec){
                        this.sku = item;
                        break;
                    }
                }
            },
            // 加入购物车
            addToCart : function () {
                // 发送跨域的异步请求(http://item.pinyougou.com --> http://cart.pinyougou.com)
                axios.get("http://cart.pinyougou.com/cart/addCart?itemId="
                    + this.sku.id + "&num=" + this.num, {withCredentials : true})
                    .then(function(response){
                        if (response.data){
                            // 跳转到购物车系统
                            location.href = "http://cart.pinyougou.com";
                        }
                });
            }
        },
        created : function () { // 创建生命周期
           // 加载默认的sku
            this.loadSku();
        }
    });
};