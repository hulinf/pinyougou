package com.pinyougou.item.listener;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.pinyougou.service.GoodsService;
import freemarker.template.Template;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Map;

/**
 * 商品静态页处理监听器
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-21<p>
 */
public class PageMessageListener implements MessageListenerConcurrently{

    @Autowired
    private FreeMarkerConfigurer freeMarkerConfigurer;
    @Reference(timeout = 10000)
    private GoodsService goodsService;
    // 商品静态页面生成的目录
    @Value("${pageDir}")
    private String pageDir;


    @Override
    public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> messageExts,
                 ConsumeConcurrentlyContext context) {
        try {
            System.out.println("=========PageMessageListener========");
            // 1. 获取消息
            MessageExt messageExt = messageExts.get(0);
            // 1.1 获取标签
            String tags = messageExt.getTags();
            // 1.2 获取消息内容
            List<Long> goodsIds = JSON.parseArray(new String(
                        messageExt.getBody(),"UTF-8"), Long.class);

            if ("CREATE".equals(tags)) { // 生成静态页面

                // 2. 生成商品的静态页面
                // 2.1 获取item.ftl模板文件对应的模板对象
                Template template = freeMarkerConfigurer.getConfiguration().getTemplate("item.ftl");
                // 2.2 循环生成多个商品的静态html页面
                for (Long goodsId : goodsIds) {
                    // 2.3 获取数据模型
                    Map<String, Object> dataModel = goodsService.getGoods(goodsId);

                    OutputStreamWriter writer = new OutputStreamWriter(
                            new FileOutputStream(pageDir + goodsId + ".html"), "UTF-8");

                    // 2.4 生成静态页面
                    template.process(dataModel, writer);

                    writer.flush();
                    writer.close();
                }
            }
            if ("DELETE".equals(tags)){ // 删除商品静态页面
                for (Long goodsId : goodsIds) {
                    File file = new File(pageDir + goodsId + ".html");
                    if (file.exists()){
                        file.delete();
                    }
                }
            }

        }catch (Exception ex){
            ex.printStackTrace();
            return ConsumeConcurrentlyStatus.RECONSUME_LATER;
        }
        return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
    }
}
