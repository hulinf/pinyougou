package com.pinyougou.item.controler;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.service.GoodsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;

/**
 * 商品详情控制器
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-20<p>
 */
@Controller
public class ItemController {

    @Reference(timeout = 10000)
    private GoodsService goodsService;

    /**
     * 请求URL: http://item.pinyougou.com/100005747076.html
     * http://item.pinyougou.com/SPU商品的id.html
     */
    @GetMapping("/{goodsId}")
    public String getGoods(@PathVariable("goodsId") Long goodsId, Model model){

        System.out.println("goodsId = " + goodsId);
        // 查询商品数据
        Map<String, Object> dataModel = goodsService.getGoods(goodsId);

        model.addAllAttributes(dataModel);
        return "item";
    }

}
