// 窗口加载完
window.onload = function () {
    var vue = new Vue({
        el : '#app', // 元素绑定
        data : { // 数据模型
            loginName : '', // 登录用户名
            redirectUrl : '', // 重定向URL
            contentList : [], // 广告数据
            keywords : '' // 搜索关键字
        },
        methods : { // 操作方法
            // 查询首页轮播广告数据
            findContentByCategoryId : function (categoryId) {
                // 发送异步请求
                axios.get("/findContentByCategoryId?categoryId="
                    + categoryId).then(function(response){
                    // 获取响应数据 List<Content>  [{},{}]
                    // {} [] [{},{}]
                    vue.contentList = response.data;
                });
            },
            // 跳转到搜索系统
            search : function () {
                location.href = "http://search.pinyougou.com/?keywords=" + this.keywords;
            },
            // 获取登录用户名
            loadUsername : function(){
                // 对请求URL进行encode编码
                this.redirectUrl = window.encodeURIComponent(location.href);
                axios.get("/user/showName").then(function(response){
                    // 获取响应数据
                    vue.loginName = response.data.loginName;
                });
            }
        },
        created : function () { // 创建生命周期
            this.loadUsername();
           // 调用查询广告数据方法
            this.findContentByCategoryId(1);
        }
    });
};