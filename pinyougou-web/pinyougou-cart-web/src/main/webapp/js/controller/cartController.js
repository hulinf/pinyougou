// 窗口加载完
window.onload = function () {
    var vue = new Vue({
        el: '#app', // 元素绑定
        data: { // 数据模型
            loginName: '', // 登录用户名
            redirectUrl: '', // 重定向URL
            carts: [], // 用户购物车数据
            totalEntity: {totalNum: 0, totalMoney: 0}, // 统计对象
            addressList: [], // 收件地址数组
            address: {}, // 收件地址
            order: {paymentType: '1'}, // 订单数据
            selectedIds: [],      //选中的商品的id
            sellerIds: [],    //选中的商户id
            checkAll: false
        },
        methods: { // 操作方法
            // 获取登录用户名h
            loadUsername: function () {
                // 对请求URL进行encode编码
                this.redirectUrl = window.encodeURIComponent(location.href);
                axios.get("/user/showName").then(function (response) {
                    // 获取响应数据
                    vue.loginName = response.data.loginName;
                });
            },
            // 查询用户的购物车
            findCart: function (needUpdateSelecteIds) {
                if (needUpdateSelecteIds == undefined) {
                    needUpdateSelecteIds = true;
                }
                axios.get("/cart/findCart").then(function (response) {
                    // 获取响应数据
                    vue.carts = response.data;
                    //清空数据
                    vue.totalEntity = {totalNum: 0, totalMoney: 0};
                    //迭代用户的购物车数组
                    for (var i = 0; i < vue.carts.length; i++) {
                        var cart = vue.carts[i];
                        for (var j = 0; j < cart.orderItems.length; j++) {
                            var orderItem = cart.orderItems[j];
                            // 统计总数量
                            // vue.totalEntity.totalNum += orderItem.num;
                            // // 统计总金额
                            // vue.totalEntity.totalMoney += orderItem.totalFee;
                            if (orderItem.selected) {
                                vue.selectedIds.push(orderItem.itemId);
                                vue.totalEntity.totalNum += orderItem.num;
                                // // 统计总金额
                                vue.totalEntity.totalMoney += orderItem.totalFee;
                            }
                        }
                    }
                    if (!needUpdateSelecteIds) {
                        this.selectedIds = [];
                    } else {
                        vue.updateInterface();
                    }
                });
            }
            ,
            // 购物车增减、删除
            addCart: function (itemId, num) {
                axios.get("/cart/addCart?itemId="
                    + itemId + "&num=" + num).then(function (response) {
                    if (response.data) {
                        // 重新查询购物车
                        vue.findCart();
                    } else {
                        alert("操作失败！");
                    }
                });
            },
            // 文本框失去焦点
            updateCart: function (itemId, num) {
                axios.get("/cart/updateCart?itemId="
                    + itemId + "&num=" + num).then(function (response) {
                    if (response.data) {
                        // 重新查询购物车
                        vue.findCart();
                    } else {
                        alert("操作失败！");
                    }
                });
            },
            // 获取收件人地址列表
            findAddressByUser: function () {
                axios.get("/order/findAddressByUser").then(function (response) {
                    // 获取响应数据
                    vue.addressList = response.data;

                    // 获取默认的收件地址
                    vue.address = vue.addressList[0];
                });
            }
            ,
            // 选择收件地址
            selectAddress: function (item) {
                this.address = item;
            }
            ,
            // 判断是否为选中的地址
            isSelectedAddress: function (item) {
                return this.address == item;
            }
            ,
            // 提交订单
            saveOrder: function () {
                // 收件人地址
                this.order.receiverAreaName = this.address.address;
                // 收件人手机号码
                this.order.receiverMobile = this.address.mobile;
                // 收件人姓名
                this.order.receiver = this.address.contact;
                // 订单来源 : pc端
                this.order.source_type = 2;
                // 发送异步请求保存订单
                axios.post("/order/save", this.order).then(function (response) {
                    if (response.data) {
                        if (vue.order.paymentType == 1) { // 在线支付
                            // 跳转到支付页面
                            location.href = "/order/pay.html";
                        } else {
                            // 跳转到成功页面
                            location.href = "/order/paysuccess.html";
                        }
                    } else {
                        alert("下单失败！");
                    }
                });
            },
            //选中当前购物车的所有商品
            selectAllCartGoods: function (cart, event) {
                var checked = event.target.checked;
                if (checked) {
                    this.sellerIds.push(cart.sellerId);
                } else {
                    this.sellerIds.splice(this.sellerIds.indexOf(cart.sellerId), 1);
                }
                var orderItems = cart.orderItems;
                for (var i = 0; i < orderItems.length; i++) {
                    var orderItem = orderItems[i];
                    if (checked) {
                        var result = this.selectedIds.find(function (itemId) {
                            return itemId == orderItem.itemId;
                        });
                        if (!result) {
                            this.selectedIds.push(orderItem.itemId);
                        }
                    } else {
                        this.selectedIds.splice(this.selectedIds.indexOf(orderItem.itemId), 1);
                    }
                }
            }
            ,
            selectAll: function (event) {
                this.sellerIds = [];
                this.selectedIds = [];
                var checked = event.target.checked;
                for (var k = 0; k < this.carts.length; k++) {
                    if (checked) {
                        this.selectAllCartGoods(this.carts[k], event);
                    }
                }
            },
            updateInterface: function () {
                this.sellerIds = [];
                this.totalEntity.totalNum = 0;
                this.totalEntity.totalMoney = 0;
                for (var k = 0; k < this.carts.length; k++) {
                    var orderItems = this.carts[k].orderItems;
                    var checked = true;     //全都选
                    for (var i = 0; i < orderItems.length; i++) {
                        var orderItem = orderItems[i];
                        var result = this.selectedIds.find(function (itemId) {
                            return itemId == orderItem.itemId;
                        });
                        if (!result) {
                            checked = false;
                        } else {
                            flag = true;
                            this.totalEntity.totalNum += orderItem.num;
                            this.totalEntity.totalMoney += orderItem.totalFee;
                        }
                    }
                    if (checked) {
                        this.sellerIds.push(this.carts[k].sellerId);
                    }
                }
                axios.post("/cart/updateSelectedOptions", vue.selectedIds).then(function (response) {
                    if (response.data) {
                        console.log("恭喜你，成功修改选中状态!");
                    } else {
                        console.log("对不起，修改选中状态失败!");
                    }
                });
            },
            isIncluded: function (cart) {
                var orderItems = cart.orderItems;
                return orderItems.find(function (item) {
                    return item.selected == true;
                });
            },
            settlement: function () {
                if (vue.selectedIds.length > 0) {
                    window.location.href = "/order/getOrderInfo.html";
                } else {
                    alert("请选择商品！")
                }
            }
        },
        created: function () { // 创建生命周期
            this.loadUsername();
            this.findCart(false);
            this.findAddressByUser();
        }
        ,
        watch: {
            'selectedIds': function (newVal, oldVal) {
                this.updateInterface();
            },
            'sellerIds': function () {
                this.checkAll = this.sellerIds.length == this.carts.length;
            }
        }
    });
};