package com.pinyougou.cart.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.pinyougou.cart.Cart;
import com.pinyougou.common.util.CookieUtils;
import com.pinyougou.pojo.OrderItem;
import com.pinyougou.service.CartService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 购物车控制器
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-26<p>
 */
@RestController
@RequestMapping("/cart")
public class CartController {

    @Reference(timeout = 10000)
    private CartService cartService;
    @Autowired
    private HttpServletRequest request;
    @Autowired(required = false)
    private HttpServletResponse response;

    /** 把SKU商品加入购物车 */
    @GetMapping("/addCart")
    @CrossOrigin(origins = "http://item.pinyougou.com",
            allowCredentials = "true") // 跨域请求注解
    public boolean addCart(Long itemId, Integer num){
        try {
            /** ###########未登录用户(把购物车存储到Cookie中)############# */
            // 1. 获取用户原来的购物车
            List<Cart> carts = findCart();

            // 2. 把商品添加到购物车，返回修改后得购物车
            carts = cartService.addItemToCart(carts, itemId, num);

            // 3. 获取登录用户
            String userId = request.getRemoteUser();
            // 判断用户是否登录
            if (StringUtils.isNoneBlank(userId)){ // 已登录
                // 3.1 把购物车存储到Redis中
               cartService.saveCartRedis(userId, carts);
            }else { // 未登录

                // 3.2 把购物车存储到Cookie中
                CookieUtils.setCookie(request, response,
                        CookieUtils.CookieName.PINYOUGOU_CART, JSON.toJSONString(carts),
                        60 * 60 * 24, true);
            }
            return true;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return false;
    }


    /** 查询用户的购物车 */
    @GetMapping("/findCart")
    public List<Cart> findCart(){

        List<Cart> carts = null;

        // 获取登录用户
        String userId = request.getRemoteUser();

        // 判断用户是否登录
        if (StringUtils.isNoneBlank(userId)){ // 已登录
            /** ###########已登录用户(从Redis中获取购物车)############# */
            carts = cartService.findCartRedis(userId);

            /** ########### 购物车合并 ########### */
            // 获取Cookie中的购物车
            String cartJsonStr = CookieUtils.getCookieValue(request,
                    CookieUtils.CookieName.PINYOUGOU_CART, true);
            if (StringUtils.isNoneBlank(cartJsonStr)){
                // 把json字符串转化成List<Cart>
                List<Cart> cookieCarts =  JSON.parseArray(cartJsonStr, Cart.class);
                // 调用服务接口合并购物车
                carts = cartService.mergeCart(cookieCarts, carts);
                // 存入Redis
                cartService.saveCartRedis(userId, carts);
                // 删除cookie中购物车
                CookieUtils.deleteCookie(request, response, CookieUtils.CookieName.PINYOUGOU_CART);
            }


        }else{ // 未登录
            /** ###########未登录用户(从Cookie中获取购物车)############# */
            // List<Cart> [{},{}]
            String cartJsonStr = CookieUtils.getCookieValue(request,
                    CookieUtils.CookieName.PINYOUGOU_CART, true);
            if (StringUtils.isBlank(cartJsonStr)){
                // 创建新的购物车
                cartJsonStr = "[]";
            }
            carts =  JSON.parseArray(cartJsonStr, Cart.class);
        }
        return carts;
    }



    /** 修改购物车 */
    @GetMapping("/updateCart")
    public boolean updateCart(Long itemId, Integer num){
        try {
            // 1. 获取用户原来的购物车
            List<Cart> carts = findCart();

            // 2. 修改购物车，返回修改后得购物车
            carts = cartService.updateCart(carts, itemId, num);

            // 3. 获取登录用户
            String userId = request.getRemoteUser();
            // 判断用户是否登录
            if (StringUtils.isNoneBlank(userId)){ // 已登录
                // 3.1 把购物车存储到Redis中
                cartService.saveCartRedis(userId, carts);
            }else { // 未登录
                // 3.2 把购物车存储到Cookie中
                CookieUtils.setCookie(request, response,
                        CookieUtils.CookieName.PINYOUGOU_CART, JSON.toJSONString(carts),
                        60 * 60 * 24, true);
            }
            return true;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return false;
    }

    @PostMapping(value = "/updateSelectedOptions")
    public boolean updateSelectedOptions(@RequestBody Long[] selectedIds) {
        try {
            // 1. 获取用户原来的购物车
            List<Cart> carts = findCart();
            for (Cart cart : carts) {
                List<OrderItem> orderItems = cart.getOrderItems();
                for (OrderItem orderItem : orderItems) {
                    boolean flag = false;
                    for (int i = 0; i < selectedIds.length; i++) {
                        if (orderItem.getItemId().equals(selectedIds[i])) {
                            orderItem.setSelected(true);
                            flag = true;
                        }
                    }
                    if (!flag) {
                        orderItem.setSelected(false);
                    }
                }
            }
            String userId = request.getRemoteUser();
            // 判断用户是否登录
            if (StringUtils.isNoneBlank(userId)) { // 已登录
                cartService.saveCartRedis(userId, carts);
            } else { // 未登录
                // 3.2 把购物车存储到Cookie中
                CookieUtils.setCookie(request, response,
                        CookieUtils.CookieName.PINYOUGOU_CART, JSON.toJSONString(carts),
                        60 * 60 * 24, true);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}