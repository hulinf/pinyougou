// 窗口加载完
window.onload = function () {
    var vue = new Vue({
        el : '#app', // 元素绑定
        data : { // 数据模型
            loginName : '' , //登录用户名
            redirectUrl: '', //重定向URL
            addressList:[],  //地址
            provincesList:[] ,//省
            areasList:[] ,//区
            citiesList:[] ,//市
            address: {
                id:'',
                alias: '',
                cityId: '', //市
                townId: '',  //区
                provinceId: '' //省
            },
            user: {headPic:''} //用户信息
        },
        methods : { // 操作方法
            // 获取登录用户名
            showName : function () {
                this.redirectUrl = window.encodeURIComponent(location.href);
                axios.get("/user/showName").then(function(response){
                    vue.loginName = response.data.loginName;
                });
            },
            clear:function () {
                vue.address={
                       id:'',
                        alias: '',
                        cityId: '', //市
                        townId: '',  //区
                        provinceId: '' //省
                }
            },
            //查询用户的地址
            findAddressByUser:function () {
                axios.get("/address/findAddressByUser").then(function (response) {
                    vue.addressList = response.data;
                });
            },
            //设为默认地址
            updateAddressByIsDefault:function (id) {
                axios.get("/address/updateAddressByIsDefault?id="+id).then(function (response) {
                    if(response.data){
                        vue.findAddressByUser();
                    }
                });
            },
            //通过主键id删除地址
            deleteAddressById:function (id,isDefault) {
                if(isDefault == 0){
                    axios.get("/address/deleteAddressById?id="+id).then(function (response) {
                        if(response.data){
                            vue.findAddressByUser();
                        }
                    });
                }else{
                    alert("默认地址不可以删除...");
                }
            },
            //省
            findprovincesList:function () {
                axios.get("/address/findprovincesList").then(function (response) {
                    if(response.data){
                        vue.provincesList = response.data;
                    }
                });
            },
            //查询市的列表
            findCitiesListByProvinceId:function (provinceId,citiesList) {
                axios.get("/address/findCitiesListByProvinceId?provinceId="+provinceId).then(function (response) {
                    if(response.data){
                        vue[citiesList] = response.data;
                    }
                });
            },
            //查询区的列表
            findAreasListByProvinceId:function (cityId,areasList) {
                axios.get("/address/findAreasListByProvinceId?cityId="+cityId).then(function (response) {
                    if(response.data){
                        vue[areasList] = response.data;
                    }
                });
            },
            //添加地址/修改
            addAddress:function () {
                var url = "addAddress";
                if(vue.address.id){
                    url="updateAddress";
                }
                axios.post("/address/"+url,this.address).then(function (response) {
                    if(response.data){
                        vue.findAddressByUser();
                    }
                });
            },
            //回显页面
            show:function (address) {
                vue.address = address;
            },
            //查询用户信息
            findUserInfo: function () {
                axios.get("/userInfo/findUserInfo").then(function (response) {
                    if (response.data) {
                        vue.user = response.data;
                    }
                });
            }
        },
        created : function () { // 创建生命周期
            this.showName();
            this.findAddressByUser();
            this.findprovincesList();
            this.findUserInfo();
        },
        //监控
        watch:{
            //监控省的id发生变化，获取市的id
            'address.provinceId':function (newVal,oldVal) {
                this.citiesList = [];
                this.areasList = [];
                vue.address.cityId='';
                vue.address.townId='';
                if (newVal) { // 不是空字符串
                    //查询市的列表
                    this.findCitiesListByProvinceId(newVal,"citiesList");
                } else {
                    this.citiesList = [];
                    this.areasList = [];
                }
            },
            //监控市的id发生变化，获取区的id
            'address.cityId':function (newVal,oldVal) {
                if (newVal) { // 不是空字符串
                    //查询区的列表
                    this.findAreasListByProvinceId(newVal,"areasList");
                } else {
                    this.areasList = [];
                }
            }


        }
    });
};