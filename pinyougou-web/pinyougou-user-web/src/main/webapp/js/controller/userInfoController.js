// 窗口加载完
window.onload = function () {
    vue = new Vue({
        el: '#app', // 元素绑定
        data: { // 数据模型
            loginName: '', //登录用户名
            redirectUrl: '', //重定向URL
            address: {
                cityId: '', //市
                townId: '',  //区
                provinceId: '' //省
            },
            provincesList: [{provinces:[]}],//省
            areasList: [{areas:[]}],//区
            citiesList: [{cities:[]}], //市
            user: {work: '',headPic:'',birthday:''} //用户信息
        },
        methods: { // 操作方法
            // 获取登录用户名
            showName: function () {
                this.redirectUrl = window.encodeURIComponent(location.href);
                axios.get("/user/showName").then(function (response) {
                    vue.loginName = response.data.loginName;
                });
            },
            //省
            findprovincesList: function () {
                axios.get("/address/findprovincesList").then(function (response) {
                    if (response.data) {
                        vue.provincesList = response.data;
                    }
                });
            },
            //查询市的列表
            findCitiesListByProvinceId: function (provinceId, citiesList) {
                axios.get("/address/findCitiesListByProvinceId?provinceId=" + provinceId).then(function (response) {
                    if (response.data) {
                        vue[citiesList] = response.data;
                    }
                });
            },
            //查询区的列表
            findAreasListByProvinceId: function (cityId, areasList) {
                axios.get("/address/findAreasListByProvinceId?cityId=" + cityId).then(function (response) {
                    if (response.data) {
                        vue[areasList] = response.data;
                    }
                });
            },
            //保存
            save: function () {
                var aaa = $("#birthday").val();
                vue.user.birthday = aaa;
                var address = JSON.stringify(vue.address);
                vue.user.address = address;
                axios.post("/userInfo/save",vue.user).then(function (response) {
                    if (response.data) {
                        vue.findUserInfo();
                    }
                });
            },
            //查询用户信息
            findUserInfo: function () {
                axios.get("/userInfo/findUserInfo").then(function (response) {
                    if (response.data) {
                        vue.user = response.data;
                        var birthday = new Date(vue.user.birthday).format("yyyy-MM-dd");
                        vue.user.birthday = birthday;

                        //{"cityId":"371300","townId":"371302","provinceId":"370000"}
                        var as = JSON.parse(vue.user.address);
                        vue.address = as ;
                        // vue.provincesList.provinces.provinceId =  as.provinceId;
                    }
                });
            },
            uploadFile:function () { //文件异步上传
                /** 创建表单对象 */
                var formData = new FormData();
                //追加需要上传的文件
                formData.append("file",file.files[0]);
                //发送异步请求上传文件
                axios({
                    method : 'post', // 请求方式
                    url : "/upload", // 请求URL
                    data : formData, // 表单数据
                    headers : {'Content-Type' : "multipart/form-data"} // 请求头
                }).then(function (response) {
                    /** 如果上传成功，取出url */
                    if(response.data.status == 200){
                        /** 设置图片访问地址 */
                        vue.user.headPic = response.data.url;
                    }else{
                        alert("上传失败！");
                    }
                });
            },
        },
        created: function () { // 创建生命周期
            this.showName();
            this.findprovincesList();
            this.findUserInfo();
        },
        //监控
        watch: {
            //监控省的id发生变化，获取市的id
            'address.provinceId': function (newVal, oldVal) {
                this.citiesList = [];
                this.areasList = [];
                // vue.address.cityId = '';
                // vue.address.townId = '';
                if (newVal) { // 不是空字符串
                    //查询市的列表
                    this.findCitiesListByProvinceId(newVal, "citiesList");
                } else {
                    this.citiesList = [];
                    this.areasList = [];
                }
            },
            //监控市的id发生变化，获取区的id
            'address.cityId': function (newVal, oldVal) {
                if (newVal) { // 不是空字符串
                    //查询区的列表
                    this.findAreasListByProvinceId(newVal, "areasList");
                } else {
                    this.areasList = [];
                }
            }


        }
    });

};

// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
// 例子：
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
Date.prototype.format = function(fmt)
{ //author: meizz
    var o = {
        "M+" : this.getMonth()+1,                 //月份
        "d+" : this.getDate(),                    //日
        "h+" : this.getHours(),                   //小时
        "m+" : this.getMinutes(),                 //分
        "s+" : this.getSeconds(),                 //秒
        "q+" : Math.floor((this.getMonth()+3)/3), //季度
        "S"  : this.getMilliseconds()             //毫秒
    };
    if(/(y+)/.test(fmt))
        fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
    for(var k in o)
        if(new RegExp("("+ k +")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
    return fmt;
}