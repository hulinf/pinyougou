// 窗口加载完
window.onload = function () {
    var vue = new Vue({
        el : '#app', // 元素绑定
        data : { // 数据模型
            loginName : '', // 登录用户名
            orders : [], // 用户订单数据
            totalEntity : {totalNum : 0, totalMoney : 0}, // 统计对象
            user: {headPic:''} //用户信息
        },
        methods : { // 操作方法
            // 获取登录用户名
            loadUsername : function () {
                axios.get("/user/showName").then(function(response){
                    vue.loginName = response.data.loginName;
                });
            },
            // 查询用户订单
            findOrders : function () {
                axios.get("/user/findOrders").then(function(response){
                    // 获取响应数据
                    vue.orders = response.data;
                    // 清空数据
                    vue.totalEntity = {totalNum : 0, totalMoney : 0};
                    // 迭代用户的订单数组
                    for (var i = 0; i < vue.orders.length; i++){
                        var order = vue.orders[i];
                        var createTime=new Date(order.createTime).format("yyyy-MM-dd");
                        order.createTime=createTime;
                        for (var j = 0; j < order.orderItems.length; j++){
                            var orderItem = order.orderItems[j];
                            // 统计
                            vue.totalEntity.totalNum += orderItem.num;
                            // 统计总金额
                            vue.totalEntity.totalMoney += orderItem.totalFee;
                        }
                    }

                });
            },
            toPay:function(outTradeNo,money){
                location.href = "/order/pay.html?outTradeNo=" + outTradeNo+"&money="+money;
            },
            //查询用户信息
            findUserInfo: function () {
                axios.get("/userInfo/findUserInfo").then(function (response) {
                    if (response.data) {
                        vue.user = response.data;
                    }
                });
            }


        },
        created : function () { // 创建生命周期
            this.loadUsername();
            this.findOrders();
            this.findUserInfo();
        }
    });
};
//日期解析方法
Date.prototype.format = function(fmt)
{ //author: meizz
    var o = {
        "M+" : this.getMonth()+1,                 //月份
        "d+" : this.getDate(),                    //日
        "h+" : this.getHours(),                   //小时
        "m+" : this.getMinutes(),                 //分
        "s+" : this.getSeconds(),                 //秒
        "q+" : Math.floor((this.getMonth()+3)/3), //季度
        "S"  : this.getMilliseconds()             //毫秒
    };
    if(/(y+)/.test(fmt))
        fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
    for(var k in o)
        if(new RegExp("("+ k +")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
    return fmt;
};