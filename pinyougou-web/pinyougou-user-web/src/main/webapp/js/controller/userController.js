// 窗口加载完
window.onload = function () {
    var vue = new Vue({
        el : '#app', // 元素绑定
        data : { // 数据模型
            user : {password : ''}, // 表单数据
            password : '', // 确认密码
            disabled : false, // 按钮是否可用
            tip : "获取短信验证码",  // 提示文本
            code : '' // 短信验证码
        },
        methods : { // 操作方法
            // 用户注册
            save : function () {
                // 判断密码是否一致
                if (this.password != '' && this.password == this.user.password){
                    axios.post("/user/save?code=" + this.code, this.user).then(function(response){
                        // 获取响应数据
                        if (response.data){
                            alert("注册成功！");
                            vue.user = {password : ''};
                            vue.password = "";
                            vue.code = "";
                        }else{
                            alert("注册失败！");
                        }
                    });
                }else {
                    alert("两次密码不一致！")
                }
            },
            // 发送短信验证码
            sendSmsCode : function () {
                if (this.user.phone != ""
                    && /^1[3|5|6|7|8|9]\d{9}$/.test(this.user.phone)){
                    axios.get("/user/sendSmsCode?phone=" + this.user.phone).then(function(response){
                        // 获取响应数据
                        if (response.data){
                            vue.downcount(90);
                        }else {
                            alert("获取短信验证码失败！");
                        }
                    });
                }else {
                    alert('手机号码格式不正确！');
                }


            },
            // 倒计时
            downcount : function (num) {
                this.disabled = true; // 不可用
                num -= 1;
                if (num >= 0) {
                    this.tip = num + "S，重新获取！";
                    // 开启定时器
                    window.setTimeout(function () {
                        vue.downcount(num);
                    }, 1000);
                }else{
                    this.disabled = false; // 可用
                    this.tip = "获取短信验证码";
                }
            }
        },
        created : function () { // 创建生命周期


           
        }
    });
};