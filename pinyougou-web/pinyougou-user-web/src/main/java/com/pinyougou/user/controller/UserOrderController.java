package com.pinyougou.user.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pojo.PayLog;
import com.pinyougou.service.OrderService;
import com.pinyougou.service.WeixinPayService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

/**
 * 订单微信支付控制器
 */
@RestController
@RequestMapping("/order")
public class UserOrderController {
    @Reference(timeout = 10000)
    private OrderService orderService;
    @Reference(timeout = 100000)
    private WeixinPayService weixinPayService;

    /** 生成微信支付二维码 */
    @GetMapping("/genPayCode")
    public Map<String,Object> genPayCode(String outTradeNo,String money){
        return weixinPayService.genPayCode(outTradeNo,Long.parseLong(money) * 100  +"");
    }
}
