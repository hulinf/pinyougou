package com.pinyougou.user.realm;

import io.buji.pac4j.realm.Pac4jRealm;
import io.buji.pac4j.subject.Pac4jPrincipal;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.subject.PrincipalCollection;

/**
 * 认证域
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-24<p>
 */
public class CasPac4jRealm extends Pac4jRealm{

    /** 授权方法 */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        Pac4jPrincipal principal = (Pac4jPrincipal)principals.getPrimaryPrincipal();
        System.out.println("登录用户名: " + principal.getName());
        // 根据用户名查询他的角色与权限

        return super.doGetAuthorizationInfo(principals);
    }
}
