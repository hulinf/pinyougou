package com.pinyougou.user.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pojo.Address;
import com.pinyougou.pojo.Areas;
import com.pinyougou.pojo.Cities;
import com.pinyougou.pojo.Provinces;
import com.pinyougou.service.AddressService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/address")
public class AddressController {

    @Reference(timeout = 10000)
    private AddressService addressService;

    @GetMapping("/findAddressByUser")
    public List<Map<String,Object>> addressList(HttpServletRequest request){

        try {
            String userId = request.getRemoteUser();
            List<Map<String, Object>> addressByUser = addressService.findAddressByUser(userId);
            for (Map<String, Object> stringObjectMap : addressByUser) {
                System.out.println("stringObjectMap.............."+stringObjectMap);
            }
            return addressByUser;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //修改默认地址
    @GetMapping("/updateAddressByIsDefault")
    public boolean updateAddressByIsDefault(Long id, HttpServletRequest request){

        try {
            String userId = request.getRemoteUser();
            addressService.updateAddressByIsDefault(id,userId);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }


    //通过主键id删除地址
    @GetMapping("/deleteAddressById")
    public boolean deleteAddressById(Long id){

        try {
            addressService.deleteAddressById(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }

    //省
    @GetMapping("/findprovincesList")
    public List<Provinces> findprovincesList(){

        try {
            return addressService.findprovincesList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    //根据省的id 查询城市列表
    @GetMapping("/findCitiesListByProvinceId")
    public List<Cities> findCitiesListByProvinceId(String provinceId ){

        try {
            return addressService.findCitiesListByProvinceId(provinceId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    //根据市的id 查询区列表
    @GetMapping("/findAreasListByProvinceId")
    public List<Areas> findAreasListByProvinceId(String cityId){

        try {
            return addressService.findAreasListByProvinceId(cityId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    //添加地址
    @PostMapping("/addAddress")
    public boolean addAddress(@RequestBody Address address, HttpServletRequest request){

        try {
            String userId = request.getRemoteUser();
            addressService.addAddress(address,userId);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }

    //修改地址
    @PostMapping("/updateAddress")
    public boolean updateAddress(@RequestBody Address address){

        try {
            addressService.updateAddress(address);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }




}
