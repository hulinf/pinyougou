package com.pinyougou.user.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pojo.User;
import com.pinyougou.service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/userInfo")
public class UserInfoController {

    @Reference
    private UserService userService;

    //查询用户信息
    @GetMapping("/findUserInfo")
    public User findUserInfo(HttpServletRequest request){
        try {
            String userId = request.getRemoteUser();
            return userService.findUserInfo(userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //保存用户信息
    @PostMapping("/save")
    public boolean save(@RequestBody User user, HttpServletRequest request){
        try {
            String userId = request.getRemoteUser();
            user.setUsername(userId);
            userService.saveUserInfo(user);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
