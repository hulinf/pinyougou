package com.pinyougou.es;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pinyougou.es.dao.EsItemDao;
import com.pinyougou.mapper.ItemMapper;
import com.pinyougou.pojo.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 数据导入到ES索引库
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-17<p>
 */
@Component
public class ItemImport {

    @Autowired
    private ItemMapper itemMapper;
    @Autowired
    private EsItemDao esItemDao;

    /** 数据导入 */
    public void importData() throws Exception{

        // 创建Item对象
        Item item = new Item();
        // 设置查询条件(正常的商品)
        item.setStatus("1");

        // 条件查询
        List<Item> itemList = itemMapper.select(item);

        System.out.println("=====华丽分割线=====");
        List<EsItem> items = new ArrayList<>();
        for (Item item1 : itemList) {
            // 把Item转化成EsItem
            EsItem esItem = new EsItem();
            esItem.setId(item1.getId());
            esItem.setTitle(item1.getTitle());
            esItem.setPrice(item1.getPrice().doubleValue());
            esItem.setImage(item1.getImage());
            esItem.setGoodsId(item1.getGoodsId());
            esItem.setCategory(item1.getCategory());
            esItem.setBrand(item1.getBrand());
            esItem.setSeller(item1.getSeller());
            // {"网络":"联通4G","机身内存":"64G"}
            String spec = item1.getSpec();
            // 用jackson把json字符串转化成map集合
            Map map = new ObjectMapper().readValue(spec, Map.class);
            // 嵌套Field
            esItem.setSpec(map);
            esItem.setUpdateTime(item1.getUpdateTime());

            items.add(esItem);
        }
        // 添加或修改索引
        esItemDao.saveAll(items);
        System.out.println("=====华丽分割线=====");
    }

    public static void main(String[] args) throws Exception {
        // 创建Spring容器
        ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
        ItemImport itemImport = ac.getBean(ItemImport.class);
        itemImport.importData();
    }

}
