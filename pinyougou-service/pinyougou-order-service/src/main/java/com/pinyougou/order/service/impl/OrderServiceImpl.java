package com.pinyougou.order.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.pinyougou.cart.Cart;
import com.pinyougou.common.util.IdWorker;
import com.pinyougou.mapper.OrderItemMapper;
import com.pinyougou.mapper.OrderMapper;
import com.pinyougou.mapper.PayLogMapper;
import com.pinyougou.pojo.Order;
import com.pinyougou.pojo.OrderItem;
import com.pinyougou.pojo.PayLog;
import com.pinyougou.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 订单服务接口实现类
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-27<p>
 */
@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private OrderItemMapper orderItemMapper;
    @Autowired
    private IdWorker idWorker;

    @Autowired
    private PayLogMapper payLogMapper;

    /** 用户下单 */
    @Override
    public void save(Order order) {
        try{
            // 用户的购物车(包含了多个商家)，按商家生成订单
            // 获取用户的购物车
            List<Cart> carts = (List<Cart>) redisTemplate
                    .boundValueOps("cart_" + order.getUserId()).get();

            // 定义支付总金额
            double totalMoney = 0;
            // 定义订单拼接的变量
            String orderIds = "";

            List<Cart> newCartList = new ArrayList<>();

            // 1. 往tb_order订单主表插入数据(一个Cart产生一个订单)

            //标志是否需要添加到新的购物车集合中
            boolean needAddToList = false;
            for (Cart cart : carts) {
                Cart newCart = new Cart();

                needAddToList = false;

                // 创建订单对象
                Order order1 = new Order();

                // 用分布式id生成器生成id
                long orderId = idWorker.nextId();
                // 订单主键id
                order1.setOrderId(orderId);
                // 支付方式
                order1.setPaymentType(order.getPaymentType());
                // 支付状态码 1、未付款
                order1.setStatus("1");
                // 订单创建时间
                order1.setCreateTime(new Date());
                // 订单更新时间
                order1.setUpdateTime(order1.getCreateTime());
                // 订单关联的用户id
                order1.setUserId(order.getUserId());
                // 收件地址
                order1.setReceiverAreaName(order.getReceiverAreaName());
                // 收件人手机号码
                order1.setReceiverMobile(order.getReceiverMobile());
                // 收件人姓名
                order1.setReceiver(order.getReceiver());
                // 订单来源
                order1.setSourceType(order.getSourceType());
                // 商家的id
                order1.setSellerId(cart.getSellerId());

                // 定义订单的总金额
                double money = 0;

                // 拼接多个订单id
                orderIds += orderId + ",";

                List<OrderItem> newOrderItems = new ArrayList<>();

                // 2. 往tb_order_item订单明细表插入数据
                for (OrderItem orderItem : cart.getOrderItems()) {
                    //若订单没有选中则不计入总金额。
                    if (!orderItem.isSelected()) {
                        newOrderItems.add(orderItem);
                        continue;
                    }
                    // 设置主键id
                    orderItem.setId(idWorker.nextId());
                    // 设置关联的订单id
                    orderItem.setOrderId(orderId);
                    // 累计
                    money += orderItem.getTotalFee().doubleValue();

                    // 保存数据
                    orderItemMapper.insertSelective(orderItem);
                }

                // 支付总金额(多个订单的总金额的累加)
                totalMoney += money;

                // 订单支付总金额
                order1.setPayment(new BigDecimal(money));
                // 保存数据
                orderMapper.insertSelective(order1);

                if (newOrderItems.size() > 0) {
                    newCart.setOrderItems(newOrderItems);
                    newCart.setSellerId(cart.getSellerId());
                    newCart.setSellerName(cart.getSellerName());
                    needAddToList = true;
                }
                if (needAddToList) {
                    newCartList.add(newCart);
                }
            }

            /** ############## 往支付日志表插入一行数据 ############# */
            if ("1".equals(order.getPaymentType())) { // 在线支付
                // 创建支付日志对象
                PayLog payLog = new PayLog();
                // 主键id
                payLog.setOutTradeNo(String.valueOf(idWorker.nextId()));
                // 创建时间
                payLog.setCreateTime(new Date());
                // 支付的总金额(分)
                payLog.setTotalFee((long) (totalMoney * 100));
                // 用户id
                payLog.setUserId(order.getUserId());
                // 交易状态码：未支付
                payLog.setTradeState("0");
                // 多个订单id，组成的字符串
                payLog.setOrderList(orderIds.substring(0, orderIds.length() - 1));
                // 支付类型
                payLog.setPayType(order.getPaymentType());

                // 插入数据
                payLogMapper.insertSelective(payLog);

                // 为了生成支付二维码查询数据方便，把支付日志存储到Redis数据库
                redisTemplate.boundValueOps("payLog_" + order.getUserId()).set(payLog);
            }

            //如果存在还未支付的订单，则需要重新存入redis
            if (newCartList.size() > 0) {
                redisTemplate.boundValueOps("cart_" + order.getUserId()).set(newCartList);
            } else {
                // 3. 从Redis中删除用户的购物车
                redisTemplate.delete("cart_" + order.getUserId());
            }


        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void update(Order order) {

    }

    @Override
    public void delete(Serializable id) {

    }

    @Override
    public void deleteAll(Serializable[] ids) {

    }

    @Override
    public Order findOne(Serializable id) {
        return null;
    }

    @Override
    public List<Order> findAll() {
        return null;
    }

    @Override
    public List<Order> findByPage(Order order, int page, int rows) {
        return null;
    }

    /**
     * 从Redis中查询该用户需要支付的订单(支付日志对象)
     */
    public PayLog findPayLogFromRedis(String userId) {
        try {
            return (PayLog) redisTemplate.boundValueOps("payLog_" + userId).get();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * 修改状态
     */
    public void updateStatus(String outTradeNo, String transactionId) {
        try {
            // 1.修改支付日志表
            PayLog payLog = payLogMapper.selectByPrimaryKey(outTradeNo);
            // 设置支付成功
            payLog.setTradeState("1");
            // 设置支付时间
            payLog.setPayTime(new Date());
            // 设置微信支付订单号
            payLog.setTransactionId(transactionId);
            // 修改
            payLogMapper.updateByPrimaryKeySelective(payLog);

            // 2.修改订单表
            String[] orderIds = payLog.getOrderList().split(",");
            for (String orderId : orderIds) {
                Order order = new Order();
                // 订单id
                order.setOrderId(Long.valueOf(orderId));
                // 设置已付款
                order.setStatus("2");
                // 设置支付时间
                order.setPaymentTime(payLog.getPayTime());
                // 修改
                orderMapper.updateByPrimaryKeySelective(order);
            }

            // 3. 从Redis数据库删除支付日志
            redisTemplate.delete("payLog_" + payLog.getUserId());
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
