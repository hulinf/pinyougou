package com.pinyougou.sellergoods.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.ISelect;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pinyougou.common.pojo.PageResult;
import com.pinyougou.mapper.SpecificationOptionMapper;
import com.pinyougou.mapper.TypeTemplateMapper;
import com.pinyougou.pojo.SpecificationOption;
import com.pinyougou.pojo.TypeTemplate;
import com.pinyougou.service.TypeTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 类型模板服务接口实现类
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-06<p>
 */
@Service
@Transactional
public class TypeTemplateServiceImpl implements TypeTemplateService {

    @Autowired
    private TypeTemplateMapper typeTemplateMapper;
    @Autowired
    private SpecificationOptionMapper specificationOptionMapper;

    @Override
    public void save(TypeTemplate typeTemplate) {
        try{
            typeTemplateMapper.insertSelective(typeTemplate);
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void update(TypeTemplate typeTemplate) {
        try{
            typeTemplateMapper.updateByPrimaryKeySelective(typeTemplate);
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void delete(Serializable id) {

    }

    @Override
    public void deleteAll(Serializable[] ids) {
        try{
            // 创建示范对象
            Example example = new Example(TypeTemplate.class);
            // 创建条件对象
            Example.Criteria criteria = example.createCriteria();
            // 添加in条件
            criteria.andIn("id", Arrays.asList(ids));
            // 条件删除
            typeTemplateMapper.deleteByExample(example);
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    @Override
    public TypeTemplate findOne(Serializable id) {
        return typeTemplateMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<TypeTemplate> findAll() {
        return null;
    }

    @Override
    public PageResult findByPage(TypeTemplate typeTemplate, int page, int rows) {

        try{
           // 开启分页
           PageInfo<Object> pageInfo = PageHelper.startPage(page, rows)
                   .doSelectPageInfo(new ISelect() {
               @Override
               public void doSelect() {
                    typeTemplateMapper.findAll(typeTemplate);
               }
           });
           return new PageResult(pageInfo.getPages(), pageInfo.getList());
        }catch (Exception ex){
           throw new RuntimeException(ex);
        }

    }

    /** 查询类型模板(id与name) */
    public List<Map<String,Object>> findTypeTemplateList(){
        try{
            return typeTemplateMapper.findTypeTemplateList();
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    /** 查询规格选项数据 */
    public List<Map> findSpecOptions(Long id){
        try{
            /**
             * List<Map<String,Object>>:
             * [{"id":27,"text":"网络", "options":[{optionName : ''},{optionName : ''}]},
                {"id":32,"text":"机身内存","options":[{optionName : ''},{optionName : ''}]}]
             */

            // 1. 根据类型模板id 查询一行数据
            TypeTemplate typeTemplate = findOne(id);
            // 2. 获取规格数据
            // [{"id":27,"text":"网络"},{"id":32,"text":"机身内存"}]
            String specIds = typeTemplate.getSpecIds();
            // 2.1. 把规格json字符串转化List集合(fastjson)
            // JSON.parseObject(); --> {}
            // JSON.parseArray();  --> []
            List<Map> specMaps = JSON.parseArray(specIds, Map.class);

            // 2.2 迭代集合
            for (Map specMap : specMaps) {
                // specMap:  {"id":27,"text":"网络"}
                Long specId = Long.valueOf(specMap.get("id").toString());

                // 查询规格对应的规格选项
                // SELECT * FROM `tb_specification_option` WHERE spec_id = 27
                SpecificationOption so = new SpecificationOption();
                so.setSpecId(specId);
                List<SpecificationOption> options = specificationOptionMapper.select(so);

                specMap.put("options", options);
            }
            return specMaps;

        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }
}
