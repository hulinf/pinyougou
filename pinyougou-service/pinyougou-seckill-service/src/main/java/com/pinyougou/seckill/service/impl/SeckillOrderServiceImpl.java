package com.pinyougou.seckill.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.pinyougou.common.util.IdWorker;
import com.pinyougou.mapper.SeckillGoodsMapper;
import com.pinyougou.mapper.SeckillOrderMapper;
import com.pinyougou.pojo.SeckillGoods;
import com.pinyougou.pojo.SeckillOrder;
import com.pinyougou.service.SeckillOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 秒杀订单服务接口实现类
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-30<p>
 */
@Service
@Transactional
public class SeckillOrderServiceImpl implements SeckillOrderService {

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private SeckillGoodsMapper seckillGoodsMapper;
    @Autowired
    private IdWorker idWorker;
    @Autowired
    private SeckillOrderMapper seckillOrderMapper;

    @Override
    public void save(SeckillOrder seckillOrder) {

    }

    @Override
    public void update(SeckillOrder seckillOrder) {

    }

    @Override
    public void delete(Serializable id) {

    }

    @Override
    public void deleteAll(Serializable[] ids) {

    }

    @Override
    public SeckillOrder findOne(Serializable id) {
        return null;
    }

    @Override
    public List<SeckillOrder> findAll() {
        return null;
    }

    @Override
    public List<SeckillOrder> findByPage(SeckillOrder seckillOrder, int page, int rows) {
        return null;
    }

    /**
     * 提交秒杀订单到Redis
     * 单进程(多线程): 线程锁(synchronized)
     * 多进程(多线程): 布布式锁(Redis) (tomcat1 tomcat2)
     * */
    public synchronized void submitOrderToRedis(String userId, Long id){
        try{
            // 获取Redis分布式锁，如果为true，就代表没有秒杀该商品
            boolean success = redisTemplate.opsForValue().setIfAbsent("kill_" + id, true);
            if (success){// 没有其他用户秒杀
                // 从Redis数据库中查询该秒杀商品
                SeckillGoods seckillGoods = (SeckillGoods) redisTemplate
                        .boundHashOps("seckillGoodsList").get(id);
                if (seckillGoods != null && seckillGoods.getStockCount() > 0){
                    // 减库存
                    seckillGoods.setStockCount(seckillGoods.getStockCount() - 1);

                    // 判断是否被秒光
                    if (seckillGoods.getStockCount() == 0){ // 秒光了
                        // 秒杀商品同步到数据库
                        seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);
                        // 从Redis中删除该秒杀商品
                        redisTemplate.boundHashOps("seckillGoodsList").delete(id);
                    }else{ // 没有秒光
                        // 把该秒杀商品同步到Redis
                        redisTemplate.boundHashOps("seckillGoodsList").put(id, seckillGoods);
                    }

                    // 产生秒杀订单
                    SeckillOrder seckillOrder = new SeckillOrder();
                    // 主键id
                    seckillOrder.setId(idWorker.nextId());
                    // 秒杀商品id
                    seckillOrder.setSeckillId(seckillGoods.getId());
                    // 秒杀商品的金额
                    seckillOrder.setMoney(seckillGoods.getCostPrice());
                    // 用户Id
                    seckillOrder.setUserId(userId);
                    // 商家id
                    seckillOrder.setSellerId(seckillGoods.getSellerId());
                    // 创建时间
                    seckillOrder.setCreateTime(new Date());
                    // 支付状态码
                    seckillOrder.setStatus("0");

                    // 把秒杀订单存储到Redis
                    redisTemplate.boundHashOps("seckillOrderList")
                            .put(userId, seckillOrder);

                    // 释放分布式锁
                    redisTemplate.delete("kill_" + id);
                }
            }else{
                throw new RuntimeException("该商品正在秒杀！");
            }
        }catch (Exception ex){
            // 释放分布式锁(怕出现死锁)
            redisTemplate.delete("kill_" + id);
            throw new RuntimeException(ex);
        }
    }

    /** 从Redis中查询该用户需要支付的秒杀订单 */
    public SeckillOrder findSeckillOrderFromRedis(String userId){
        try{
            return (SeckillOrder)redisTemplate
                    .boundHashOps("seckillOrderList").get(userId);
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }


    /** 保存秒杀订单 */
    public void saveOrder(String userId, String transactionId){
        try{
            // 1. 从Redis数据库获取秒杀订单
            SeckillOrder seckillOrder = findSeckillOrderFromRedis(userId);

            // 2. 同步到数据库
            seckillOrder.setTransactionId(transactionId);
            seckillOrder.setPayTime(new Date());
            seckillOrder.setStatus("1");
            seckillOrderMapper.insertSelective(seckillOrder);


            // 3. 从Redis数据库删除秒杀订单
            redisTemplate.boundHashOps("seckillOrderList").delete(userId);
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    /** 查询超时5分钟未支付的秒杀订单 */
    public List<SeckillOrder> findOrderByTimeout(){
        try{
            // 定义集合封装超时的秒杀订单
            List<SeckillOrder> seckillOrders = new ArrayList<>();

            // 查询全部未支付的秒杀订单
            List<SeckillOrder> seckillOrderList = redisTemplate
                    .boundHashOps("seckillOrderList").values();

            for (SeckillOrder seckillOrder : seckillOrderList) {
                // 判断哪些秒杀订单超出了5分钟(当前系统时间  - 5分钟)
                long date = new Date().getTime() - (5 * 60 * 1000);
                if (seckillOrder.getCreateTime().getTime() < date){
                    seckillOrders.add(seckillOrder);
                }
            }
            return seckillOrders;
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    /** 删除超时未支付的秒杀订单，恢复库存 */
    public void deleteOrderFromRedis(SeckillOrder seckillOrder){
        try{
            // 1. 从Redis数据库中删除秒杀订单
            redisTemplate.boundHashOps("seckillOrderList")
                    .delete(seckillOrder.getUserId());

            // 2. 恢复库存
            // 2.1 从Redis中查询秒杀商品
            SeckillGoods seckillGoods = (SeckillGoods) redisTemplate.boundHashOps("seckillGoodsList")
                    .get(seckillOrder.getSeckillId());
            // 2.2 判断是否为空
            if (seckillGoods != null){
                // 增加库存
                seckillGoods.setStockCount(seckillGoods.getStockCount() + 1);
            }else{
                // 从数据库查询秒杀商品
                seckillGoods = seckillGoodsMapper.selectByPrimaryKey(seckillOrder.getSeckillId());
                seckillGoods.setStockCount(1);
                // 同步到数据库
                seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);
            }

            // 2.3 重新存储到Redis数据库
            redisTemplate.boundHashOps("seckillGoodsList")
                    .put(seckillGoods.getId(), seckillGoods);

        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }
}
