package com.pinyougou.sms.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.pinyougou.service.SmsService;
import org.springframework.beans.factory.annotation.Value;

import java.util.Map;

/**
 * 短信服务接口实现类
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-23<p>
 */
@Service
public class SmsServiceImpl implements SmsService {

    // 产品域名
    private static final String DOMAIN = "dysmsapi.aliyuncs.com";
    // 签名KEY
    @Value("${sms.accessKeyId}")
    private String accessKeyId;
    // 签名密钥
    @Value("${sms.accessKeySecret}")
    private String accessKeySecret;

    /**
     * 短信发送的方法
     * @param phone 手机号码
     * @param signName 短信签名
     * @param templateCode 模板代号
     * @param templateParam 模板变量参数(json字符串)
     * @return true : 发送成功  false : 发送失败
     */
    public boolean sendSms(String phone, String signName,
                    String templateCode, String templateParam){
        try{
            DefaultProfile profile = DefaultProfile.getProfile("default",
                    accessKeyId, accessKeySecret);
            IAcsClient client = new DefaultAcsClient(profile);
            // 封装请求参数对象
            CommonRequest request = new CommonRequest();
            // 请求方式
            request.setMethod(MethodType.POST);
            // 请求域名
            request.setDomain(DOMAIN);
            // 版本号
            request.setVersion("2017-05-25");
            // 请求URL
            request.setAction("SendSms");
            // 手机号码
            request.putQueryParameter("PhoneNumbers", phone);
            // 短信签名
            request.putQueryParameter("SignName", signName);
            // 短信模板
            request.putQueryParameter("TemplateCode", templateCode);
            // 模板中的参数
            request.putQueryParameter("TemplateParam", templateParam);

            // 响应对象
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
            // {"Message":"OK","RequestId":"69CB255D-BA39-4E22-AD2D-55564109719C",
            // "BizId":"762113761277121951^0","Code":"OK"}
            Map<String,String> data = JSON.parseObject(response.getData(), Map.class);

            return "OK".equals(data.get("Code"));
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }
}