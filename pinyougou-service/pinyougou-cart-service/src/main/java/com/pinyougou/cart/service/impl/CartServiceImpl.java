package com.pinyougou.cart.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.pinyougou.cart.Cart;
import com.pinyougou.mapper.ItemMapper;
import com.pinyougou.pojo.Item;
import com.pinyougou.pojo.OrderItem;
import com.pinyougou.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 购物车服务接口实现类
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-26<p>
 */
@Service
@Transactional
public class CartServiceImpl implements CartService {

    @Autowired
    private ItemMapper itemMapper;
    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * 把SKU商品添加到购物车
     * @param carts 购物车集合
     * @param itemId SKU商品的id
     * @param num 购买数量
     * @return 修改后的购物车集合
     */
    public List<Cart> addItemToCart(List<Cart> carts, Long itemId, Integer num){
        try{
            // 1. 根据SKU商品id从tb_item表查询一个商品
            Item item = itemMapper.selectByPrimaryKey(itemId);

            // 2. 从用户的购物车集合中获取该商品对应的商家购物车
            Cart cart = searchCartBySellerId(carts, item.getSellerId());

            // 3. 判断商家的购物车
            if (cart == null){ // 代表用户没有购买过该商家的商品
                // 3.1 创建商家的购物车
                cart = new Cart();
                // 3.2 设置商家id
                cart.setSellerId(item.getSellerId());
                // 3.3 设置商家的名称
                cart.setSellerName(item.getSeller());
                // 3.4 创建商家的商品的集合
                List<OrderItem> orderItems = new ArrayList<>();
                // 3.5 创建购买的商品
                OrderItem orderItem = createOrderItem(item, num);
                // 3.6 添加一个商品到商品的集合中
                orderItems.add(orderItem);
                // 3.7 设置购买的商品集合
                cart.setOrderItems(orderItems);
                // 3.8 添加到用户的购物车集合中
                carts.add(cart);

            }else { // 代表用户购买过该商家的商品
                // 4.1 获取该商家的商品集合
                List<OrderItem> orderItems = cart.getOrderItems();

                // 4.2 从商家的商品集合找商品
                OrderItem orderItem = searchOrderItemByItemId(orderItems, itemId);

                // 4.3 判断是否购买过同样的商品
                if (orderItem == null){ // 没买过同样的商品
                    orderItem = createOrderItem(item, num);
                    orderItems.add(orderItem);
                }else { // 买过同样的商品
                    // 修改购买数量
                    orderItem.setNum(orderItem.getNum() + num);
                    // 修改小计金额
                    orderItem.setTotalFee(new BigDecimal(orderItem.
                            getPrice().doubleValue() * orderItem.getNum()));

                    // 从商家的购物车中删除商品
                    if (orderItem.getNum() == 0){
                        orderItems.remove(orderItem);
                    }
                    // 从用户的购物车集合中删除商家的购物车
                    if (orderItems.size() == 0){
                        carts.remove(cart);
                    }
                }
            }

            return carts;
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    /** 从商家的商品集合找商品 */
    private OrderItem searchOrderItemByItemId(List<OrderItem> orderItems, Long itemId) {
        for (OrderItem orderItem : orderItems) {
            if (orderItem.getItemId().equals(itemId)){
                return orderItem;
            }
        }
        return null;
    }

    /** 创建商家的购物车商品 */
    private OrderItem createOrderItem(Item item, Integer num) {
        OrderItem orderItem = new OrderItem();
        // SKU的id
        orderItem.setItemId(item.getId());
        // SPU的id
        orderItem.setGoodsId(item.getGoodsId());
        // 商品标题
        orderItem.setTitle(item.getTitle());
        // 商品价格
        orderItem.setPrice(item.getPrice());
        // 购买数量
        orderItem.setNum(num);
        // 小计金额
        orderItem.setTotalFee(new BigDecimal(item.getPrice().doubleValue() * num));
        // 商品图片
        orderItem.setPicPath(item.getImage());
        // 商家的id
        orderItem.setSellerId(item.getSellerId());
        return orderItem;

    }

    /** 从用户的购物车集合中获取该商品对应的商家购物车 */
    private Cart searchCartBySellerId(List<Cart> carts, String sellerId) {
        for (Cart cart : carts) {
            if (cart.getSellerId().equals(sellerId)){
                return cart;
            }
        }
        return null;
    }


    /** 修改购物车 */
    public List<Cart> updateCart(List<Cart> carts, Long itemId, Integer num) {
        try {
            // 1. 根据SKU商品id从tb_item表查询一个商品
            Item item = itemMapper.selectByPrimaryKey(itemId);

            // 2. 从用户的购物车集合中获取该商品对应的商家购物车
            Cart cart = searchCartBySellerId(carts, item.getSellerId());

            if (cart != null) {
                // 4.1 获取该商家的商品集合
                List<OrderItem> orderItems = cart.getOrderItems();
                // 4.2 从商家的商品集合找商品
                OrderItem orderItem = searchOrderItemByItemId(orderItems, itemId);
                // 4.3 修改购买数量
                orderItem.setNum(num);
                // 4.4 修改小计金额
                orderItem.setTotalFee(new BigDecimal(orderItem.
                        getPrice().doubleValue() * orderItem.getNum()));
            }
            return carts;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /** 用户购物车存储到Redis */
    public void saveCartRedis(String userId, List<Cart> carts){
        try{
            redisTemplate.boundValueOps("cart_" + userId).set(carts);
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    /** 从Redis数据库获取购物车 */
    public List<Cart> findCartRedis(String userId){
        try{
            List<Cart> carts = (List<Cart>)redisTemplate
                    .boundValueOps("cart_" + userId).get();
            if (carts == null) {
                carts = new ArrayList<>();
            }
            return carts;
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    /**
     * 购物车合并
     * @param cookieCarts Cookie中的购物车
     * @param redisCarts Redis中的购物车
     * @return 合并后的购物车
     */
    public List<Cart> mergeCart(List<Cart> cookieCarts, List<Cart> redisCarts){
        try{
            for (Cart cookieCart : cookieCarts) {
                for (OrderItem orderItem : cookieCart.getOrderItems()) {
                    redisCarts = addItemToCart(redisCarts,
                            orderItem.getItemId(), orderItem.getNum());
                }
            }
            return redisCarts;
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }
}