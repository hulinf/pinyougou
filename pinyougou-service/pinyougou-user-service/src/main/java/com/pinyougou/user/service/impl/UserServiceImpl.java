package com.pinyougou.user.service.impl;

import java.util.Date;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.pinyougou.common.util.HttpClientUtils;
import com.pinyougou.mapper.OrderItemMapper;
import com.pinyougou.mapper.OrderMapper;
import com.pinyougou.mapper.UserMapper;
import com.pinyougou.pojo.Order;
import com.pinyougou.pojo.OrderItem;
import com.pinyougou.pojo.User;
import com.pinyougou.service.UserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 用户服务接口实现类
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-23<p>
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private OrderItemMapper orderItemMapper;

    @Value("${sms.url}")
    private String smsUrl;
    @Value("${sms.signName}")
    private String signName;
    @Value("${sms.templateCode}")
    private String templateCode;

    @Override
    public void save(User user) {
        try{
            // 密码加密 MD5 (commons-codec.xxx.jar)
            user.setPassword(DigestUtils.md5Hex(user.getPassword()));
            // 设置创建时间
            user.setCreated(new Date());
            // 设置修改时间
            user.setUpdated(user.getCreated());
            // 添加
            userMapper.insertSelective(user);
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void update(User user) {

    }

    @Override
    public void delete(Serializable id) {

    }

    @Override
    public void deleteAll(Serializable[] ids) {

    }

    @Override
    public User findOne(Serializable id) {
        return null;
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public List<User> findByPage(User user, int page, int rows) {
        return null;
    }

    /**
     * 发送短信验证码
     */
    public boolean sendSmsCode(String phone) {
        try {
            // 1. 生成六位数字的验证码
            String code = UUID.randomUUID().toString().replaceAll("-", "")
                    .replaceAll("[a-z|A-Z]", "").substring(0, 6);
            System.out.println("code = " + code);

            // 2. 调用短信发送接口
            // 创建HttpClientUtils对象
            HttpClientUtils httpClientUtils = new HttpClientUtils(false);
            // 定义Map集合封装请求参数
            Map<String, String> params = new HashMap<>();
            params.put("phone", phone);
            params.put("signName", signName);
            params.put("templateCode", templateCode);
            params.put("templateParam", "{'number' : '" + code + "'}");

            // 发送post请求
            String res = httpClientUtils.sendPost(smsUrl, params);
            Map data = JSON.parseObject(res, Map.class);

            // 3. 判断短信是否发送成功
            boolean success = (boolean) data.get("success");
            if (success) {
                // 4. 把短信验证码存储到Redis数据库(有效时间90秒)
                redisTemplate.boundValueOps(phone).set(code, 90, TimeUnit.SECONDS);
            }

            return success;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * 检验短信验证码
     */
    public boolean checkSmsCode(String phone, String code) {
        try {
            // 从Redis数据库中获取验证码
            String oldCode = (String) redisTemplate.boundValueOps(phone).get();
            return oldCode != null && oldCode.equals(code);

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    // 查询所有的订单
    @Override
    public List<Order> findOrders(String userId) {
//        定义一个集合用来封装所有的订单
        List<Order> userOrders=new ArrayList<>();
//        根据用户id查询用户所有的订单
        List<Order> orderLists=orderMapper.selectByUserId(userId);
        for (Order orderList : orderLists) {
            Order order = new Order();
            order.setOrderId(orderList.getOrderId());
            order.setPayment(orderList.getPayment());
            order.setPaymentType(orderList.getPaymentType());
            order.setStatus(orderList.getStatus());
            order.setCreateTime(orderList.getCreateTime());
            order.setUpdateTime(orderList.getUpdateTime());
            order.setPaymentTime(orderList.getPaymentTime());
            order.setConsignTime(new Date());
            order.setUserId(orderList.getUserId());
            order.setReceiverAreaName(orderList.getReceiverAreaName());
            order.setReceiverMobile(orderList.getReceiverMobile());
            order.setReceiver(orderList.getReceiver());
            order.setSellerId(orderList.getSellerId());
//            查询所有的订单详情
            List<OrderItem> orderItemList = orderItemMapper.selectByOrderId(orderList.getOrderId());
//            调用方法获取相同订单编号下的订单详情
            List<OrderItem> orderItems = findAllOrderItems(orderList.getOrderId(), orderItemList);
            order.setOrderItems(orderItems);
//           将订单详情添加到订单对象中
            userOrders.add(order);
        }
        return userOrders;
    }
    //查询用户信息
    @Override
    public User findUserInfo(String userId) {
        try {
            User user = new User();
            user.setUsername(userId);
            return userMapper.selectOne(user);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    //保存用户信息
    @Override
    public void saveUserInfo(User user) {
        try {
            user.setSourceType("1");
            userMapper.updateByPrimaryKeySelective(user);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    //  获取相同订单编号下的订单详情
    private List<OrderItem> findAllOrderItems(Long orderId, List<OrderItem> orderItemList) {
        List<OrderItem> list = new ArrayList<>();
        for (OrderItem orderItem : orderItemList) {
            if (orderItem.getOrderId().equals(orderId)) {
                list.add(orderItem);
            }
        }
        return list;
    }


}