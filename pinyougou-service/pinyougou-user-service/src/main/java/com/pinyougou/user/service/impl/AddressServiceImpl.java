package com.pinyougou.user.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.pinyougou.mapper.AddressMapper;
import com.pinyougou.mapper.AreasMapper;
import com.pinyougou.mapper.CitiesMapper;
import com.pinyougou.mapper.ProvincesMapper;
import com.pinyougou.pojo.Address;
import com.pinyougou.pojo.Areas;
import com.pinyougou.pojo.Cities;
import com.pinyougou.pojo.Provinces;
import com.pinyougou.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 收件地址服务接品口实现类
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-27<p>
 */
@Service
@Transactional
public class AddressServiceImpl implements AddressService {

    @Autowired
    private AddressMapper addressMapper;
    @Autowired
    private ProvincesMapper provincesMapper;
    @Autowired
    private CitiesMapper citiesMapper;
    @Autowired
    private AreasMapper areasMapper;

    @Override
    public void save(Address address) {
    }


    @Override
    public void update(Address address) {

    }

    @Override
    public void delete(Serializable id) {

    }

    @Override
    public void deleteAll(Serializable[] ids) {

    }

    @Override
    public Address findOne(Serializable id) {
        return null;
    }

    @Override
    public List<Address> findAll() {
        return null;
    }

    @Override
    public List<Address> findByPage(Address address, int page, int rows) {
        return null;
    }

    // 根据用户编号查询地址
    @Override
    public  List<Map<String,Object>> findAddressByUser(String userId) {

        try {
            return addressMapper.findAddressByUser(userId);
        } catch (Exception var4) {
            throw new RuntimeException(var4);
        }
    }

    /**
     *  修改默认地址
     * @param id 主键id
     * @param userId  用户id
     */
    @Override
    public void updateAddressByIsDefault(Long id, String userId) {
        try {
            ///修改用户为userId 和 当前isDefault为1 ，修改 isDefault ：0
            //UPDATE tb_address SET is_default = '0' WHERE user_id ='lufei' AND is_default='1'
            Example example = new Example(Address.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("userId", userId);
            criteria.andEqualTo("isDefault", "1");

            Address address = new Address();
            address.setIsDefault("0");
            addressMapper.updateByExampleSelective(address, example);

            //修改用户为userId 和 主键是 id 的信息，修改 isDefault ：1
            // UPDATE tb_address SET is_default = '1' WHERE user_id ='lufei' AND id=66

            Example example1 = new Example(Address.class);
            Example.Criteria criteria1 = example1.createCriteria();
            criteria1.andEqualTo("userId", userId);
            criteria1.andEqualTo("id", id);
            address = new Address();
            address.setIsDefault("1");
            addressMapper.updateByExampleSelective(address, example1);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    //通过主键id删除地址
    @Override
    public void deleteAddressById(Long id) {

        try {
            //delete from tb_address where id = 68
            addressMapper.deleteByPrimaryKey(id);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    //省
    @Override
    public List<Provinces> findprovincesList() {

        try {
            return  provincesMapper.selectAll();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    //根据省的id 查询城市id
    @Override
    public List<Cities> findCitiesListByProvinceId(String provinceId) {

        try {
            //SELECT * FROM tb_cities WHERE provinceid= '130000'
            Example example = new Example(Cities.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("provinceId",provinceId);
            return citiesMapper.selectByExample(example);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    //根据市的id 查询区列表
    @Override
    public List<Areas> findAreasListByProvinceId(String cityId) {
        try {
            //SELECT * FROM tb_areas WHERE cityid= '130000'
            Example example = new Example(Areas.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("cityId",cityId);
            return areasMapper.selectByExample(example);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    //添加地址
    @Override
    public void addAddress(Address address,String userId) {
        try {
            address.setIsDefault("0");
            address.setUserId(userId);
            address.setCreateDate(new Date());
            addressMapper.insertSelective(address);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    //修改地址
    @Override
    public void updateAddress(Address address) {
        try {
            addressMapper.updateByPrimaryKeySelective(address);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Provinces findprovinceName(String provinceId) {

        try {
            Provinces provinces = new Provinces();
            provinces.setProvinceId(provinceId);
            return provincesMapper.selectOne(provinces);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Areas findtownName(String townId) {
        try {
            Areas areas = new Areas();
            areas.setAreaId(townId);
            return areasMapper.selectOne(areas);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Cities findcityName(String cityId) {
        try {
            Cities cities = new Cities();
            cities.setCityId(cityId);
            return citiesMapper.selectOne(cities);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
