package com.pinyougou.search.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.pinyougou.es.EsItem;
import com.pinyougou.search.dao.EsItemDao;
import com.pinyougou.service.ItemSearchService;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.SearchResultMapper;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;
import org.springframework.data.elasticsearch.core.query.DeleteQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 商品服务接口实现类
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-17<p>
 */
@Service
public class ItemSearchServiceImpl implements ItemSearchService {

    @Autowired
    private ElasticsearchTemplate esTemplate;
    @Autowired
    private EsItemDao esItemDao;


    /** 添加或修改商品索引 */
    public void saveOrUpdate(List<EsItem> esItems){
        try{
            esItemDao.saveAll(esItems);
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    /** 删除商品索引 */
    public void delete(List<Long> goodsIds){
        try{
            // 创建删除的查询对象
            DeleteQuery query = new DeleteQuery();
            // 设置索引库
            query.setIndex("pinyougou");
            // 设置类型(表)
            query.setType("item");
            // 设置删除条件
            query.setQuery(QueryBuilders.termsQuery("goodsId", goodsIds));
            // 条件删除
            esTemplate.delete(query);
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    /** 搜索方法 */
    @Override
    public Map<String, Object> search(Map<String, Object> params) {
        try{

            // 创建原生的搜索查询构建对象
            NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
            // 设置搜索条件：查询全部
            builder.withQuery(QueryBuilders.matchAllQuery());

            // 获取搜索关键字
            String keywords = (String)params.get("keywords");
            // 判断关键字是否为空
            if (StringUtils.isNoneBlank(keywords)){ // 高亮显示

                /**#################### 1.搜索高亮 ###################*/
                // 1.1 设置多条件匹配查询
                builder.withQuery(QueryBuilders.multiMatchQuery(keywords,
                        "title","category","brand","seller"));

                // 1.2 创建高亮字段
                HighlightBuilder.Field field = new HighlightBuilder
                        .Field("title") // 高亮字段
                        .preTags("<font color='red'>") // 高亮前缀
                        .postTags("</font>")  // 高亮后缀
                        .fragmentSize(30); // 文本截断

                // 1.3 原生的搜索查询构建对象设置高亮字段
                builder.withHighlightFields(field);
            }


            /**#################### 2.搜索过滤 ###################*/
            // 2.1 创建组合查询构建对象，用于组合多个过滤条件
            BoolQueryBuilder bqBuilder = new BoolQueryBuilder();

            // { "keywords": "", "category": "手机", "brand": "苹果",
            //   "spec": { "网络": "移动3G", "机身内存": "64G" }, "price": "1000-1500" }
            // 2.2 用bqBuilder组合多个过滤条件
            // 2.2.1 商品分类过滤
            String category = (String)params.get("category");
            if (StringUtils.isNoneBlank(category)){
                bqBuilder.must(QueryBuilders.termQuery("category", category));
            }

            // 2.2.2 商品品牌过滤
            String brand = (String)params.get("brand");
            if (StringUtils.isNoneBlank(brand)){
                bqBuilder.must(QueryBuilders.termQuery("brand", brand));
            }

            // 2.2.3 商品规格选项过滤
            // "spec": { "网络": "移动3G", "机身内存": "64G" }
            Map<String,String> spec = (Map<String, String>) params.get("spec");
            if (spec != null && spec.size() > 0) {
                // 迭代规格选项(添加多个过滤查询)
                for (String key : spec.keySet()) {
                    // 定义嵌套Field的名称
                    String field = "spec." + key + ".keyword";
                    // 添加过滤查询(使用嵌套查询)
                    // String path, QueryBuilder query, ScoreMode scoreMode
                    bqBuilder.must(QueryBuilders.nestedQuery("spec",
                            QueryBuilders.termsQuery(field, spec.get(key)), ScoreMode.Max));
                }
            }

            // 2.2.4 商品价格过滤
            // "price": "0-500" 、"1000-1500"、"3000-*"
            String price = (String)params.get("price");
            if (StringUtils.isNoneBlank(price)){
                // 把字符串，分隔成数组
                String[] priceArr = price.split("-");

                // 创建范围查询构建对象
                RangeQueryBuilder rqBuidler = new RangeQueryBuilder("price");
                // 判断数组的第二个元素是否为星号
                if ("*".equals(priceArr[1])){
                    // 价格大于3000
                    rqBuidler.gt(priceArr[0]);
                }else {
                    // 从哪里开始  到 哪里结束
                    rqBuidler.from(priceArr[0], true).to(priceArr[1], true);
                }
                // 添加价格过滤
                bqBuilder.must(rqBuidler);
            }

            // 2.3 设置过滤查询
            builder.withFilter(bqBuilder);



            // 创建搜索查询对象
            SearchQuery query = builder.build();
            /**#################### 3.搜索分页 ###################*/
            // 获取当前页码
            Integer currPage = (Integer) params.get("page");
            if (currPage == null || currPage < 1){
                currPage = 1;
            }
            // 设置分页对象
            query.setPageable(PageRequest.of((currPage - 1), 10));


            /**#################### 4.搜索排序 ###################*/
            // 获取排序参数
            String sortField = (String)params.get("sortField");
            String sortValue = (String)params.get("sortValue");
            if (StringUtils.isNoneBlank(sortField)
                    && StringUtils.isNoneBlank(sortValue)) {
                // 创建排序对象
                Sort sort = new Sort("ASC".equals(sortValue)
                        ? Sort.Direction.ASC : Sort.Direction.DESC, sortField);
                // 添加排序
                query.addSort(sort);
            }



            // 分页搜索，得到合计分页对象
            AggregatedPage<EsItem> page = esTemplate.queryForPage(query, EsItem.class,
                    new SearchResultMapper() { // 搜索结果转化
                @Override
                public <T> AggregatedPage<T> mapResults(SearchResponse sr,
                                                        Class<T> aClass, Pageable pageable) {
                    // 定义List集合封装搜索到的分页数据
                    List<T> content = new ArrayList<>();
                    // 迭代搜索的结果
                    for (SearchHit hit : sr.getHits()) {
                        // hit：封装一篇文档
                        // 获取该文档对应的json字符串，把json字符串转化成EsItem
                        EsItem esItem = JSON.parseObject(hit.getSourceAsString(), EsItem.class);
                        // 获取标题高亮字段
                        HighlightField highlightField = hit.getHighlightFields().get("title");
                        // 判断标题高亮字段是否为空
                        if (highlightField != null){
                            // 获取标题的高亮内容
                            String title = highlightField.getFragments()[0].toString();
                            System.out.println("title = " + title);
                            // 设置标题高亮内容
                            esItem.setTitle(title);
                        }
                        // 添加到集合
                        content.add((T)esItem);
                    }
                    // List<T> content: 分页数据
                    // Pageable pageable： 分页对象
                    // long total: 总记录数
                    return new AggregatedPageImpl<T>(content, pageable, sr.getHits().getTotalHits());
                }
            });

            // 创建Map集合封装返回的数据
            Map<String, Object> data = new HashMap<>();
            // 总记录数
            data.put("total", page.getTotalElements());
            // 分页数据
            data.put("rows", page.getContent());
            // 总页数
            data.put("totalPages", page.getTotalPages());

            return data;
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }

    }
}