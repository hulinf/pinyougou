package com.pinyougou.sms;

import com.pinyougou.common.util.HttpClientUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 短信接口调用测试
 *
 * @author lee.siu.wah
 * @version 1.0
 * <p>File Created at 2019-06-23<p>
 */
public class SmsTest {

    public static void main(String[] args){
        // 创建HttpClientUtils对象
        HttpClientUtils httpClientUtils = new HttpClientUtils(false);
        // 定义Map集合封装请求参数
        Map<String, String> params = new HashMap<>();
        params.put("phone", "15914264552");
        params.put("signName", "五子连珠");
        params.put("templateCode", "SMS_11480310");
        params.put("templateParam", "{'number' : '666688'}");

        // 发送post请求
        String res = httpClientUtils.sendPost("http://sms.pinyougou.com/sms/sendSms", params);
        System.out.println("res = " + res);
    }

}
