package com.pinyougou.mapper;

import tk.mybatis.mapper.common.Mapper;

import com.pinyougou.pojo.Address;

import java.util.List;
import java.util.Map;

/**
 * AddressMapper 数据访问接口
 * @date 2019-06-05 15:27:23
 * @version 1.0
 */
public interface AddressMapper extends Mapper<Address>{


    List<Map<String,Object>> findAddressByUser(String userId);
}