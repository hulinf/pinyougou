package com.pinyougou.mapper;

import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import com.pinyougou.pojo.OrderItem;

import java.util.List;

/**
 * OrderItemMapper 数据访问接口
 * @date 2019-06-05 15:27:23
 * @version 1.0
 */
public interface OrderItemMapper extends Mapper<OrderItem>{

//  根据订单id查询所有用户的订单详情
    @Select("select * from tb_order_item where order_id=#{orderId}")
    List<OrderItem> selectByOrderId(Long orderId);
}