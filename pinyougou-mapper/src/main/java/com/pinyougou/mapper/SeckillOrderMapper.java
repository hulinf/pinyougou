package com.pinyougou.mapper;

import tk.mybatis.mapper.common.Mapper;

import com.pinyougou.pojo.SeckillOrder;

/**
 * SeckillOrderMapper 数据访问接口
 * @date 2019-06-05 15:27:23
 * @version 1.0
 */
public interface SeckillOrderMapper extends Mapper<SeckillOrder>{



}