package com.pinyougou.mapper;

import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import com.pinyougou.pojo.Order;

import java.util.List;

/**
 * OrderMapper 数据访问接口
 * @date 2019-06-05 15:27:23
 * @version 1.0
 */
public interface OrderMapper extends Mapper<Order>{


    List<Order> selectByUserId(String userId);
}